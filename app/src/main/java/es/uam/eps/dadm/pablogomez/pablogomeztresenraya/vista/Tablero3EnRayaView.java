/**
 * Vista del tablero 3 En Raya. Se encarga de mostrar la partida tal cual se encuentra en cada momento
 * a traves del metodo onDraw de la misma
 *
 * @author  Pablo Gomez Delgado
 * @version 1.6
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Shader;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.OnCasillaSeleccionadaListener;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Ficha;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Tablero3EnRaya;

/**
 * Creado por Pablo Gomez el 04/03/2016.
 */
public class Tablero3EnRayaView extends View{

    private Tablero3EnRaya tablero;
    private OnCasillaSeleccionadaListener listener;
    private TipoJuego tipo;
    private Bitmap ficha1_sw;
    private Bitmap ficha2_sw;
    private Paint borde;
    private Shader shadeBorde2;
    private int alto;
    private int ancho;
    private Paint ficha2;
    private Paint ficha1;
    private Rect posicion;
    private float[] posiciones;


    /* Constructores de la clase View */
    public Tablero3EnRayaView(Context context) {
        this(context, null, 0);
    }

    public Tablero3EnRayaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Tablero3EnRayaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Metodo que inicializa todos los atributos necesarios para pintar la vista con OnDraw
     */
    private void init(){

        /* Obtenemos las dimensiones del control */
        alto = getMeasuredHeight();
        ancho = getMeasuredWidth();

        ficha1 = new Paint();
        ficha2 = new Paint();
        borde = new Paint();
        posicion = new Rect(0,0,0,0);
        posiciones = new float[4];

        int colores[] = {ContextCompat.getColor(getContext(), R.color.alpha),
                ContextCompat.getColor(getContext(), R.color.azulclaro),
                ContextCompat.getColor(getContext(), R.color.alpha)};

        Shader shadeBorde1 = new LinearGradient(ancho / 3, 0, ancho / 3, alto, colores, null, Shader.TileMode.MIRROR);
        shadeBorde2 = new LinearGradient(0, alto / 3, ancho, alto / 3, colores, null, Shader.TileMode.MIRROR);

        /* Diseño de las lineas con las que se va a dibujar el tablero */
        borde.setStyle(Paint.Style.STROKE);
        borde.setShader(shadeBorde1);
        borde.setStrokeWidth(6);

        if(tipo == TipoJuego.SIMPLE) {
            ficha1.setStyle(Paint.Style.STROKE);
            ficha1.setStrokeWidth(8);
            ficha1.setColor(ContextCompat.getColor(getContext(), R.color.azulomedio));

            ficha2.setStyle(Paint.Style.STROKE);
            ficha2.setStrokeWidth(8);
            ficha2.setColor(ContextCompat.getColor(getContext(), R.color.granate));
        }
        else{

            ficha1_sw = BitmapFactory.decodeResource(getResources(), R.drawable.luke);
            ficha2_sw = BitmapFactory.decodeResource(getResources(), R.drawable.vader);
        }
    }

    /**
    * Metodo que pinta en la pantalla el tablero con sus fichas
    * Parte de Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
     * @param canvas El lienzo sobre el que pintar
    */
    @Override
    protected void onDraw(Canvas canvas) {

        /* Dibujamos las lineas verticales */
        canvas.drawLine(ancho / 3, 0, ancho / 3, alto, borde);
        canvas.drawLine(2 * ancho / 3, 0, 2 * ancho / 3, alto, borde);

        borde.setShader(shadeBorde2);
        /* Dibujamos las lineas horizontales */
        canvas.drawLine(0, alto / 3, ancho, alto / 3, borde);
        canvas.drawLine(0, 2 * alto / 3, ancho, 2 * alto / 3, borde);


        for(int fila = 0; fila < 3; fila ++) {
            for(int columna = 0; columna < 3; columna ++) {

                posiciones[0] = columna * (ancho / 3) + (ancho / 3) * 0.25f;
                posiciones[1] = fila * (alto / 3) + (alto / 3) * 0.25f;
                posiciones[2] = columna * (ancho / 3) + (ancho / 3) * 0.75f;
                posiciones[3] = fila * (alto / 3) + (alto / 3) * 0.75f;


                if(tablero.getFichaPosicion(fila, columna).equals(Ficha.TIPO1)) {
                    if(tipo == TipoJuego.SIMPLE) {

                        canvas.drawLine(
                                posiciones[0],
                                posiciones[1],
                                posiciones[2],
                                posiciones[3],
                                ficha1);

                        canvas.drawLine(
                                posiciones[2],
                                posiciones[1],
                                posiciones[0],
                                posiciones[3],
                                ficha1);
                    }
                    else{

                        posicion.set(
                                (int)posiciones[0],
                                (int)posiciones[1],
                                (int)posiciones[2],
                                (int)posiciones[3]);

                        canvas.drawBitmap(ficha1_sw, null, posicion, null);

                    }

                }
                else if(tablero.getFichaPosicion(fila, columna).equals(Ficha.TIPO2)) {

                    if(tipo == TipoJuego.SIMPLE){
                        canvas.drawCircle(
                                columna * (ancho / 3) + (ancho / 6),
                                fila * (alto / 3) + (alto / 6),
                                (ancho / 6) * 0.5f, ficha2);

                    }
                    else{
                        posicion.set(
                                (int)posiciones[0],
                                (int)posiciones[1],
                                (int)posiciones[2],
                                (int)posiciones[3]);

                        canvas.drawBitmap(ficha2_sw, null, posicion, null);
                    }

                }
            }
        }
    }
    /**
    * Metodo para mantener la proporcion de la vista
    * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
    */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int ancho = calcularAncho(widthMeasureSpec);
        int alto = calcularAlto(heightMeasureSpec);

        if(ancho < alto)
            alto = ancho;
        else
            ancho = alto;

        setMeasuredDimension(ancho, alto);
    }
    /**
    * Metodo para calcular el alto de la vista
    * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
    */
    private int calcularAlto(int limitesSpec)
    {
        int res = 100; //Alto por defecto

        int modo = MeasureSpec.getMode(limitesSpec);
        int limite = MeasureSpec.getSize(limitesSpec);

        if (modo == MeasureSpec.AT_MOST) {
            res = limite;
        }
        else if (modo == MeasureSpec.EXACTLY) {
            res = limite;
        }

        return res;
    }
    /**
    * Metodo para calcular el ancho de la vista
    * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
    */
    private int calcularAncho(int limitesSpec)
    {
        int res = 100; //Ancho por defecto

        int modo = MeasureSpec.getMode(limitesSpec);
        int limite = MeasureSpec.getSize(limitesSpec);

        if (modo == MeasureSpec.AT_MOST) {
            res = limite;
        }
        else if (modo == MeasureSpec.EXACTLY) {
            res = limite;
        }

        return res;
    }

    /**
     * Reescribimos onLayout debido a que se llama despues de onMeasure, con lo que nos aseguramos
     * de tener ya las dimensiones de la pantalla (ancho y alto) bien recogidas
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        init();
    }

    /**
     * Listener del toque de pantalla
     * @param event Evento que ha provocado la accion
     * @return booleano que indica si se ha realizado con exito
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {

       if (event.getAction() == MotionEvent.ACTION_UP) {

            int fil = (int) (event.getY() / (alto / 3));
            int col = (int) (event.getX() / (ancho / 3));

            /* Lanzamos el evento de pulsacion */
            if (listener != null) {
                listener.onCasillaSeleccionada(fil, col);
            }
        }

        return true;
    }

    /**
     * Setter del listener de la casilla
     * @param l el listener de la casilla
     */
    public void setOnCasillaSeleccionadaListener(OnCasillaSeleccionadaListener l) {
        listener = l;
    }

    /**
     * Setter del tablero de la vista
     * @param tablero El tablero a mostrar
     */
    public void setTablero (Tablero3EnRaya tablero) { this.tablero = tablero; }

    /**
     * Setter del tipo de juego
     * @param tipo El tipo de juego a ejecutar
     */
    public void setTipo(TipoJuego tipo){
        this.tipo = tipo;
        init();

    }

    /**
     * Getter del tipo de juego. Devuelve una variable
     * @return TipoJuego el tipo de juego al que se esta jugando
     */
    public TipoJuego getTipo(){
        return tipo;
    }

    /**
     * Getter del tablero de la vista
     * @return El tablero de la vista
     */
    public Tablero3EnRaya getTablero() {
        return tablero;
    }
}
