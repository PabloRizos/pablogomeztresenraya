package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

/**
 * Clase que implementa la funcionalidad basica de la Base de Datos a traves de un patrón Repository
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   02-04-2016
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.text.format.DateFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class MiDB implements Repository {

    private final String DEBUG_TAG = "DEBUG";
    private MiDataBaseHelper helper;
    private SQLiteDatabase db;

    public MiDB(Context context) {
        helper = new MiDataBaseHelper(context);
    }

    @Override
    public void open() throws SQLException {
        db = helper.getWritableDatabase();
    }
    @Override
    public void close() {
        db.close();
    }
    @Override
    public void login(String playername, String password, LoginRegisterCallback callback) {
        Cursor cursor = db.query(TablaUsuario.TABLE_NAME,
                new String[] { TablaUsuario.UUID },
                TablaUsuario.USERNAME + " = ? AND " + TablaUsuario.PASSWORD + " = ?",
                new String[] { playername, password }, null, null, null);
        int count = cursor.getCount();
        String uuid = count == 1 && cursor.moveToFirst() ? cursor.getString(0) : "";
        cursor.close();

        if (count == 1)
            callback.onLogin(uuid);
        else
            callback.onError("No existe el usuario o contraseña");
    }
    @Override
    public void register(String playername, String password, LoginRegisterCallback callback) {

        ContentValues values = new ContentValues();
        String uuid = UUID.randomUUID().toString();
        values.put(TablaUsuario.PASSWORD, password);
        values.put(TablaUsuario.USERNAME, playername);
        values.put(TablaUsuario.UUID, uuid);
        long id = db.insert(TablaUsuario.TABLE_NAME, null, values);
        if (id < 0)
            callback.onError("ERROR: ");
        else
            callback.onLogin(uuid);

    }
    @Override
    public void addResult(String playeruuid, int time, int points, String otherInfo, BooleanCallback callback) {
        ContentValues values = new ContentValues();

        /** Cogemos el ID de partida máaximo, oséa el ultimo ID añadido */
        Cursor cursorPartida = db.query(TablaPartida.TABLE_NAME,
                new String[] {"MAX(" + TablaPartida.PARTIDA + ")" },
                TablaPartida.ID + " = '" + playeruuid + "'", null, TablaPartida.ID, null,
                TablaPartida.PARTIDA);

        int maxPartida = cursorPartida.moveToFirst() ? cursorPartida.getInt(0) : 0;

        /** Recogemos la fecha actual */
        Date fecha = new Date();
        CharSequence fechaChar = DateFormat.format("YYY-MM-DD HH:MM", fecha.getTime());

        values.put(TablaPartida.ID, playeruuid);
        values.put(TablaPartida.DURACION, time);
        values.put(TablaPartida.NPIEZAS, points);
        values.put(TablaPartida.PARTIDA, maxPartida + 1);
        values.put(TablaPartida.FECHA, (String)fechaChar);
        values.put(TablaPartida.ESTADO_PARTIDA, otherInfo);
        long id = db.insert(TablaPartida.TABLE_NAME, null, values);

        cursorPartida.close();

        if (id < 0)
            callback.onResponse(false);
        else
            callback.onResponse(true);
    }

    @Override
    public void getResults(String playeruuid, String orderByField, String group, ResultsCallback callback) {

        /** Array de resultados a devolver */
        ArrayList<Result> list = new ArrayList<>();

        /** Consulta sql para la obtencion de resultados */
        Cursor cursor = db.rawQuery("SELECT " + TablaUsuario.USERNAME + ","
                + TablaPartida.PARTIDA + ","
                + TablaPartida.DURACION + ","
                + TablaPartida.NPIEZAS + ","
                + TablaPartida.ESTADO_PARTIDA + " "
                + "FROM " + TablaPartida.TABLE_NAME + " AS r ,"
                + TablaUsuario.TABLE_NAME + " AS u "
                + "WHERE r." + TablaPartida.ID + "=u." + TablaUsuario.UUID , null);

        /** bucle que itera en cada resultado */
        while (cursor.moveToNext()) {

            Result result = new Result();

            int iName = cursor.getColumnIndex(TablaUsuario.USERNAME);
            int iDuracion = cursor.getColumnIndex(TablaPartida.DURACION);
            int iNPiezas = cursor.getColumnIndex(TablaPartida.NPIEZAS);
            int iEstado = cursor.getColumnIndex(TablaPartida.ESTADO_PARTIDA);

            result.playerName = cursor.getString(iName);
            result.otherInfo = cursor.getString(iEstado);
            result.points = cursor.getInt(iNPiezas);
            result.time = cursor.getInt(iDuracion);

            /** Añadimos el resultado a la lista creada */
            list.add(result);
        }

        callback.onResponse(list);

        /** Liberamos el cursor */
        cursor.close();

    }

    @Override
    public void getOpenRounds(String playerUUID, PartidaJSONCallback callback) {

    }

    @Override
    public void newRound(String playerid, StringResponseCallback callback) {

    }

}
