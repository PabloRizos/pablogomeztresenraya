package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;

import java.util.HashMap;
import java.util.Map;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.PrincipalActivity;

/**
 *
 */
public class InterfazConServidor {

    private static final String BASE_URL = "http://ptha.ii.uam.es/dadm2016/";
    private static final String ACCOUNT_PHP = "account.php";
    private static final String OPEN_ROUNDS_PHP = "openrounds.php";
    private static final String ADD_SCORE_PHP = "addscore.php";
    private static final String ROUND_HISTORY_PHP = "roundhistory.php";
    private static final String ACTIVE_ROUNDS_PHP = "activerounds.php";
    private static final String FINISHED_ROUNDS_PHP = "finishedrounds.php";
    private static final String NEW_ROUND_PHP = "newround.php";
    private static final String ADD_PLAYER_ROUND_PHP = "addplayertoround.php";
    private static final String REMOVE_PLAYER_ROUND_PHP = "removeplayerfromround.php";
    private static final String IS_MY_TURN_PHP = "ismyturn.php";
    private static final String NEW_MOVEMENT_PHP = "newmovement.php";
    private static final String SEND_MSG_PHP = "sendmsg.php";
    private static final String GET_MSG_PHP = "getmsg.php";


    private static final String DEBUG_TAG = "InterfazConServidor";
    public RequestQueue queue;
    private static InterfazConServidor serverInterface;

    private InterfazConServidor(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public static InterfazConServidor getServer(Context context) {
        if (serverInterface == null) {
            serverInterface = new InterfazConServidor(context);
        }
        return serverInterface;
    }

    /**
     * Metodo que da de alta un usuario en una base de datos remota
     * @param playername El nombre del jugador
     * @param playerpassword La contraseña del jugador
     * @param callback El callback de exito
     * @param errorCallback El callback de error
     */
    public void account(final String playername, final String playerpassword,
                        Response.Listener<String> callback,
                        Response.ErrorListener errorCallback) {
        String url = BASE_URL + ACCOUNT_PHP;
        Log.d(DEBUG_TAG, url);
        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("playername", playername);
                params.put("playerpassword", playerpassword);
                return params;
            }
        };
        queue.add(request);
    }

    /**
     * Metodo que conecta para crear el login
     * @param playername El nombre del jugador
     * @param playerpassword La contraseña del jugador
     * @param callback El callback de exito
     * @param errorCallback El callback de error
     */
    public void login(final String playername,
                      final String playerpassword,
                      Response.Listener<String> callback,
                      Response.ErrorListener errorCallback) {
        String url = BASE_URL + ACCOUNT_PHP;
        Log.d(DEBUG_TAG, url);
        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("playername", playername);
                params.put("playerpassword", playerpassword);
                params.put("login", "");
                return params;
            }
        };
        queue.add(request);
    }

    /**
     * Metodo que devuelve las rondas abiertas en formato JSON
     * @param playerid El id del jugador
     * @param callback El callback de exito
     * @param errorCallback El callback de error
     */
    public void openRounds(final String playerid,
                      Response.Listener<JSONArray> callback,
                      Response.ErrorListener errorCallback) {

        String url = BASE_URL + OPEN_ROUNDS_PHP + "?gameid=" + PrincipalActivity.GAME_ID +
                "&playerid=" + playerid;

        Log.d(DEBUG_TAG, url);
        JsonArrayRequest request = new JsonArrayRequest(url, callback, errorCallback);
        queue.add(request);
    }

    public void newRound(final String playerid, final String codeboard, Response.Listener<String> callback,
                         Response.ErrorListener errorCallback) {

        String url = BASE_URL + NEW_ROUND_PHP;
        Log.d(DEBUG_TAG, url);
        StringRequest request = new StringRequest(Request.Method.POST, url, callback, errorCallback) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("playerid", playerid);
                params.put("gameid", PrincipalActivity.GAME_ID);
                if (codeboard != null)
                    params.put("codeboard", codeboard);
                return params;
            }
        };
        queue.add(request);
    }
}