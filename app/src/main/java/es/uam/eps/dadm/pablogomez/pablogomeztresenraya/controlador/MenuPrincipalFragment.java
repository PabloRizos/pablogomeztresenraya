/**
 * Fragmento encargado de mostrar el menu principal y recoger la opcion seleccionada
 * en el caso de que se haya presionado uno de los distintos botones
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;


public class MenuPrincipalFragment extends Fragment {

    private OnBotonSeleccionadoListener listener;


    public MenuPrincipalFragment() {
        // Required empty public constructor
    }

    /**
     * Metodo que se ejecuta al crear una vista
     * @param inflater El inflador de la vista
     * @param container Un contenedor de vistas
     * @param savedInstanceState El estado actual o guardado
     * @return Devuelve la vista inflada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista =  inflater.inflate(R.layout.activity_control_menu, container, false);

        /** Cargamos el archivo de audio para el efecto de click */
        final MediaPlayer efectoClick;
        efectoClick = MediaPlayer.create(getActivity(), R.raw.boton_push);

        /** Recogemos el valor de los efectos de sonido */
        final boolean isEfectos = PrincipalActivity.getValorEfectos(getActivity());

        /** Recogemos los botones y les asignamos sus listeners */
        Button botonNuevaPartida = (Button) vista.findViewById(R.id.boton_nuevapartida);
        botonNuevaPartida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.NUEVA_PARTIDA);
                if(isEfectos)
                    efectoClick.start();
            }
        });

        Button botonCargarPartida = (Button) vista.findViewById(R.id.boton_cargarpartida);
        botonCargarPartida.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.CARGAR_PARTIDA);
                if(isEfectos)
                    efectoClick.start();
            }
        });

        if(PrincipalActivity.getLocalState(getActivity()))
            botonCargarPartida.setEnabled(true);
        else
            botonCargarPartida.setEnabled(false);

        Button botonPartidasOnline = (Button) vista.findViewById(R.id.boton_listaonline);
        botonPartidasOnline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.PARTIDAS_ONLINE);
                if (isEfectos)
                    efectoClick.start();
            }
        });

        if(PrincipalActivity.getLocalState(getActivity()))
            botonPartidasOnline.setEnabled(false);
        else
            botonPartidasOnline.setEnabled(true);

        Button botonPreferencias = (Button) vista.findViewById(R.id.boton_preferencias);
        botonPreferencias.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.PREFERENCIAS);
                if(isEfectos)
                    efectoClick.start();
            }
        });

        Button botonEstadisticas = (Button) vista.findViewById(R.id.boton_estadisticas);
        botonEstadisticas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.ESTADISTICAS);
                if(isEfectos)
                    efectoClick.start();
            }
        });

        Button botonInstrucciones = (Button) vista.findViewById(R.id.boton_instrucciones);
        botonInstrucciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onBotonSeleccionado(TipoBoton.INSTRUCCIONES);
                if(isEfectos)
                    efectoClick.start();
            }
        });
        return vista;

    }
    /**
     * Sobreescritura del metodo onAttach para settear el listener del fragmento
     * @param savedInstanceState Contexto de la actividad
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Activity actividad = getActivity();
        if (actividad instanceof OnBotonSeleccionadoListener)
            listener = (OnBotonSeleccionadoListener) actividad;
        else {
            throw new ClassCastException(actividad.toString() +
                    " no implementa OnButtonSelectedListener");
        }
    }

    /**
     * Sobreescritura del metodo onDetach para settear el listener del fragmento a null
     */
    @Override
    public void onDetach(){
        super.onDetach();
        listener = null;
    }
}
