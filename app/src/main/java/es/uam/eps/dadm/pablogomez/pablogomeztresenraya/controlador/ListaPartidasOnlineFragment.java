package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import java.util.ArrayList;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.MiArrayAdapterOnline;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListaPartidasOnlineFragment extends Fragment {

    private static ListView listAbiertas;
    private ListView listEnCuros;
    private ArrayAdapter<Repository.PartidaJSON> abiertasAdapter;
    private ArrayAdapter<Repository.PartidaJSON> encursoAdapter;
    private Repository db;
    private boolean abiertas = true;
    private Repository.PartidaJSONCallback callbackAbiertas;
    private Repository.PartidaJSONCallback callbackEnCurso;

    public ListaPartidasOnlineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_lista_partidas_online, container, false);

        abiertasAdapter = new MiArrayAdapterOnline(getActivity());
        encursoAdapter = new MiArrayAdapterOnline(getActivity());

        listAbiertas = (ListView) vista.findViewById(R.id.listaabiertas);
        listEnCuros = (ListView) vista.findViewById(R.id.listaencurso);

        /* Iniciamos la reproduccion de la musica solo si en las preferencias esta activada */
        /** Cargamos el archivo de audio para el efecto de click */
        final MediaPlayer efectoClick;
        efectoClick = MediaPlayer.create(getActivity(), R.raw.boton_push);

        /** Funcionalidad de las pestañas */
        TabHost tabs = (TabHost) vista.findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec(getResources().getString(R.string.lista));
        spec.setContent(R.id.tab1);
        spec.setIndicator(getResources().getString(R.string.abiertas));
        tabs.addTab(spec);

        spec = tabs.newTabSpec(getResources().getString(R.string.encurso));
        spec.setContent(R.id.tab2);
        spec.setIndicator(getResources().getString(R.string.encurso));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);


        db = RepositoryFactory.createRepository(getActivity());
        callbackAbiertas = new Repository.PartidaJSONCallback() {
            @Override
            public void onResponse(ArrayList<Repository.PartidaJSON> partidas) {

                if(partidas != null) {
                    Log.i("DEBUG :::::", "ENTRO EN ONRESPONSE DEL CALLBACK");
                    Log.i("DEBUG ::::: ", partidas.toString());
                    for (Repository.PartidaJSON partida : partidas)
                        abiertasAdapter.add(partida);
                }
                listAbiertas.setAdapter(abiertasAdapter);
            }

            @Override
            public void OnError(String error) {

                Toast.makeText(getActivity(), "No se han podido listar las partidas: " + error, Toast.LENGTH_LONG).show();
            }
        };

        callbackEnCurso = new Repository.PartidaJSONCallback() {
            @Override
            public void onResponse(ArrayList<Repository.PartidaJSON> partidas) {
                for (Repository.PartidaJSON partida : partidas)
                    encursoAdapter.add(partida);

                listEnCuros.setAdapter(encursoAdapter);
            }

            @Override
            public void OnError(String error) {
                Toast.makeText(getActivity(), "No se han podido listar las partidas: " + error, Toast.LENGTH_LONG).show();

            }
        };

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {

                Log.i("AndroidTabsDemo", "Pulsada pestaña: " + tabId);
                if (PrincipalActivity.getValorEfectos(getActivity()))
                    efectoClick.start();
            }
        });

        db.getOpenRounds(PrincipalActivity.getPlayerUUID(getActivity()), callbackAbiertas);

        return vista;
    }

    /**
     * Getter estatico del atributo list view de las abiertas
     * @return El list view
     */
    public static ListView getListViewAbiertas(){
        return listAbiertas;
    }

}
