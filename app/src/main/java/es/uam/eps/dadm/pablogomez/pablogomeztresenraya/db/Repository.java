package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created
 * @author gonzalo on 27/01/16.
 */
public interface Repository {

    void open() throws Exception;
    void close();

    interface LoginRegisterCallback {
        void onLogin(String playerUuid);
        void onError(String error);
    }
    interface StringResponseCallback {
        void onResponse(String response);
        void onError(String error);
    }
    void login(String playername, String password, LoginRegisterCallback callback);
    void register(String playername, String password, LoginRegisterCallback callback);

    interface BooleanCallback {
        void onResponse(boolean ok);
    }

    void addResult(String playeruuid, int time, int points, String otherInfo, BooleanCallback callback);

    class Result {
        public String playerName;
        public int time;
        public int points;
        public String otherInfo;
    }

    class PartidaJSON {
        public Integer roundid;
        public int numberofplayer;
        public String time;
        public String playernames;
        public int turn;
        public String codeboard;
    }

    interface ResultsCallback {
        void onResponse(ArrayList<Result> results);
        void onError(String error);
    }

    interface PartidaJSONCallback{
        void onResponse(ArrayList<PartidaJSON> partidas);
        void OnError(String error);
    }

    /**
     *   La implementación de esta función es libre, en el sentido que se pueden ignorar
     *   los parámetros o utilizarlos cómo se prefiera.
     *
     *   Una forma de usarlo podría ser:
     *      playeruuid :   Cuando se especifíque se devolveran los resultados ese usuario
     *                     solo
     *      orderByField : Cuando se especifíque se devolveran los resultados ordenados por
     *                     ese campo
     *      group:         Cuando se especifíque se devolveran los resultados agregados
     */
    void getResults(String playeruuid, String orderByField, String group, ResultsCallback callback);


    /**
     * Creacion de nuevos metodos
     */

    /**
     * Devuelve las partidas abiertas
     * @param playerUUID El id del jugador
     * @param callback El callback de respuesta
     */
    void getOpenRounds(String playerUUID, PartidaJSONCallback callback);

    /**
     * Devuelve el identificador de la partida creada
     * @param playerid El id del jugador
     * @param callback El callback de devolucion
     */
    void newRound(String playerid, StringResponseCallback callback);

}
