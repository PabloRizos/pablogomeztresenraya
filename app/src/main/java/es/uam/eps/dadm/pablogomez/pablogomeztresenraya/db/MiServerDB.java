package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.text.DateFormat;
import java.util.ArrayList;

public class MiServerDB implements Repository {

    private InterfazConServidor helper;
    private static MiServerDB miServerDBInstance;


    private MiServerDB(Context context) {
        helper = InterfazConServidor.getServer(context);
    }

    public static MiServerDB getInstance(Context context){

        if(miServerDBInstance == null)
            miServerDBInstance = new MiServerDB(context);
        return miServerDBInstance;


    }
    @Override
    public void open() throws Exception {
        /** NADA */

    }

    @Override
    public void close() {
        /** NADA */
    }

    @Override
    public void login(String playername, String password, final LoginRegisterCallback callback) {

        /**
         * Creamos un nuevo callback de respuesta
         */
        Response.Listener<String> callbackOk = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("-1"))
                    callback.onError(response);
                else
                    callback.onLogin(response);
            }
        };

        /**
         * Creamos un nuevo callback de error
         */
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());

            }
        };

        /** Llamamos al account del servidor remoto */
        helper.login(playername, password, callbackOk, errorCallback);

    }

    @Override
    public void register(String playername, String password, final LoginRegisterCallback callback) {

        /**
         * Creamos un nuevo callback de respuesta
         */
        Response.Listener<String> callbackOk = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("-1"))
                    callback.onError(response);
                else
                    callback.onLogin(response);
                Log.v("DEGUB", "Player UUIF register: " + response);
            }
        };

        /**
         * Creamos un nuevo callback de error
         */
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.getMessage());
                Log.v("DEGUB", "Player ERROR: " + error.getMessage());

            }
        };

        /** Llamamos al account del servidor remoto */
        helper.account(playername, password, callbackOk, errorCallback);
    }

    @Override
    public void addResult(String playeruuid, int time, int points, String otherInfo, BooleanCallback callback) {

    }

    @Override
    public void getResults(String playeruuid, String orderByField, String group, ResultsCallback callback) {

    }

    @Override
    public void getOpenRounds(String playerUUID, final PartidaJSONCallback callback) {

        Response.Listener<JSONArray> callbackJSON = new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<PartidaJSON> partidas = null;
                try {
                    partidas = procesaJSONArray(response);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                callback.onResponse(partidas);
            }
        };
        /**
         * Creamos un nuevo callback de error
         */
        Response.ErrorListener errorCallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                callback.OnError(error.getMessage());

            }
        };

        helper.openRounds(playerUUID, callbackJSON, errorCallback);
    }

    @Override
    public void newRound(String playerid, final StringResponseCallback partida) {

        Response.Listener<String> callback = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if(response.equals("-1"))
                    partida.onError(response);
                else
                    partida.onResponse(response);
            }
        };

        Response.ErrorListener errorcallback = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                partida.onError(error.getMessage());
            }
        };

        helper.newRound(playerid, null, callback, errorcallback);
    }

    /**
     * Procesa el JSON devuelto por un callback
     * @param response JSONArray que contiene las partidas en tipo JSON
     * @return Las partidas parseadas
     * @throws JSONException
     */
    private ArrayList<PartidaJSON> procesaJSONArray (JSONArray response) throws JSONException {

        ArrayList<PartidaJSON> partidas = new ArrayList<>();
        PartidaJSON partida;

        for (int i=0; i<response.length(); i++){

            partida = new PartidaJSON();

            JSONObject partidaJSON = (JSONObject) response.get(i);

            partida.roundid = partidaJSON.getInt("roundid");
            partida.numberofplayer = partidaJSON.getInt("numberofplayers");
            partida.time = partidaJSON.getString("dateevent");
            partida.playernames = partidaJSON.getString("playernames");
            partida.turn = partidaJSON.getInt("turn");
            //partida.codeboard = partidaJSON.getString("codeboard");

            partidas.add(partida);
        }

        return partidas;
    }

}
