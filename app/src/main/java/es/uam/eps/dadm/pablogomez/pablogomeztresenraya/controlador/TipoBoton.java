package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;

/**
 * Enumeracion con los tipos de botones que se pueden pulsar en la aplicacion
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   25-03-2016
 */
public enum TipoBoton {

    NUEVA_PARTIDA, CARGAR_PARTIDA, PREFERENCIAS, ESTADISTICAS, INSTRUCCIONES, PARTIDAS_ONLINE

}
