package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.TipoJuego;

/**
 * Interfaz que se define como un listener, que define un metodo onBotonSeleccionado() para la
 * comunicacion entre los fragmentos y la actividad principal.
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   26-03-2016
 */

public interface OnBotonSeleccionadoListener {
    void onBotonSeleccionado(TipoBoton seleccion);
}
