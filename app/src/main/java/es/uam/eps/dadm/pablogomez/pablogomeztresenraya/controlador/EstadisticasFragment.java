/**
 * Fragmento que se encarga de mostrar las estadisticas del jugador, del juego y del uso de la
 * aplicacion
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   28-03-2016
 *
 * El codigo referente a la representacion de estadisticas proviene
 * de la libreria MPAndroidChart del repositorio de GitHub de PhilJay:
 *
 *      Autor: PhilJay
 *      Codigo: https://github.com/PhilJay/MPAndroidChart
 *      Wiki: https://github.com/PhilJay/MPAndroidChart/wiki
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;

import java.util.ArrayList;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.TablaPartida;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.MiArrayAdapter;


public class EstadisticasFragment extends Fragment {

    private ArrayAdapter<Repository.Result> milista;
    private ListView lv;

    public EstadisticasFragment() {
        // Required empty public constructor
    }


    /**
     * Metodo que muestra las estadisticas de la partida
     * @param inflater Inflador de la vista
     * @param container Contenedor del grupo de vista
     * @param savedInstanceState Instancia salvada anteriormente
     * @return La vista creada
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        /* Inflate the layout for this fragment */
        final View vista = inflater.inflate(R.layout.fragment_estadisticas, container, false);

        lv = (ListView) vista.findViewById(R.id.listaestats);
        milista = new MiArrayAdapter(getActivity());

        /** Recuperacion de la base de datos */
        Repository repository = RepositoryFactory.createRepository(getActivity());
        Repository.ResultsCallback callback = new Repository.ResultsCallback() {
            @Override
            public void onResponse(ArrayList<Repository.Result> results) {
                if(results.isEmpty())
                for(Repository.Result r : results) {
                    milista.add(r);
                    Log.i("Resultados", r.otherInfo);
                }
                TextView listVacia = (TextView) vista.findViewById(R.id.textlistvacio);
                lv.setEmptyView(listVacia);
                lv.setAdapter(milista);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), R.string.errorcargaestats, Toast.LENGTH_SHORT).show();
            }
        };

        assert repository != null;
            repository.getResults(PrincipalActivity.getPlayerUUID(getActivity()), TablaPartida.FECHA, null, callback);

        /* Iniciamos la reproduccion de la musica solo si en las preferencias esta activada */
        /** Cargamos el archivo de audio para el efecto de click */
        final MediaPlayer efectoClick;
        efectoClick = MediaPlayer.create(getActivity(), R.raw.boton_push);

        /** Funcionalidad de las pestañas */
        TabHost tabs = (TabHost) vista.findViewById(android.R.id.tabhost);
        tabs.setup();

        TabHost.TabSpec spec = tabs.newTabSpec(getResources().getString(R.string.lista));
        spec.setContent(R.id.tab1);
        spec.setIndicator(getResources().getString(R.string.lista));
        tabs.addTab(spec);

        spec = tabs.newTabSpec(getResources().getString(R.string.grafico));
        spec.setContent(R.id.tab2);
        spec.setIndicator(getResources().getString(R.string.grafico));
        tabs.addTab(spec);

        tabs.setCurrentTab(0);

        tabs.setOnTabChangedListener(new TabHost.OnTabChangeListener() {
            @Override
            public void onTabChanged(String tabId) {
                Log.i("AndroidTabsDemo", "Pulsada pestaña: " + tabId);
                if(PrincipalActivity.getValorEfectos(getActivity()))
                    efectoClick.start();
            }
        });


        /** Funcionalidad de los gráficos */
        PieChart estats = (PieChart) vista.findViewById(R.id.estadisticas);
        estats.setDescription(getResources().getString(R.string.textocentroestats));
        estats.setHoleColor(ContextCompat.getColor(getActivity(), R.color.alpha));

        float[] resultados = cuentaPartidas();

        if(resultados[0] == 0 && resultados[1] == 0 && resultados[2]==0)
            estats.setCenterText(getResources().getString(R.string.nopartidas));
        else {
            float resTotal = resultados[0] + resultados[1] + resultados[2];
            estats.setCenterText(getResources().getString(R.string.totalpartidas) + " " + String.valueOf(resTotal));
        }

        ArrayList<Entry> entradas = new ArrayList<>();
        /** 0 0 1 */
        if(resultados[0]==0 && resultados[1]==0 && resultados[2]!=0)
            entradas.add(new Entry(resultados[2], 0));
        /** 0 1 0 */
        else if(resultados[0]==0 && resultados[1]!=0 && resultados[2]==0)
            entradas.add(new Entry(resultados[1], 0));
        /** 0 1 1 */
        else if(resultados[0]==0 && resultados[1]!=0 && resultados[2]!=0){
            entradas.add(new Entry(resultados[1], 0));
            entradas.add(new Entry(resultados[2], 1));
        }
        /** 1 0 0 */
        else if(resultados[0]!=0 && resultados[1]==0 && resultados[2]==0)
            entradas.add(new Entry(resultados[0], 0));
        /** 1 0 1 */
        else if(resultados[0]!=0 && resultados[1]==0 && resultados[2]!=0){
            entradas.add(new Entry(resultados[0], 0));
            entradas.add(new Entry(resultados[2], 1));
        }
        /** 1 1 0 */
        else if(resultados[0]!=0 && resultados[1]!=0 && resultados[2]==0){
            entradas.add(new Entry(resultados[0], 0));
            entradas.add(new Entry(resultados[1], 1));
        }

        entradas.add(new Entry(resultados[2], 2));

        PieDataSet dataset = new PieDataSet(entradas, null);

        int colores[] = {ContextCompat.getColor(getActivity(), R.color.granateclaro),
                ContextCompat.getColor(getActivity(), R.color.azulomedio),
                ContextCompat.getColor(getActivity(), R.color.verdeclaro)};

        dataset.setColors(colores);
        ArrayList<String> xVals = new ArrayList<>();
        xVals.add(getResources().getString(R.string.perdidas));
        xVals.add(getResources().getString(R.string.empatadas));
        xVals.add(getResources().getString(R.string.ganadas));

        PieData datos = new PieData(xVals, dataset);
        datos.setHighlightEnabled(true);
        estats.setData(datos);
        estats.invalidate();

        return vista;
    }

    /**
     * Metodo que cuenta la cantidad de ganadas, perdidas y empatadas que hay
     * @return Array de enteros que tiene en la primera posicion las perdidas, en la segunda las
     * empatadas y en la tercera las ganadas
     */
    private float[] cuentaPartidas(){

        float[] resultado = {0, 0, 0};

        for(int i=0; i<milista.getCount(); i++){
            String estado = milista.getItem(i).otherInfo;
            String[] splitted = estado.split("\\|");
            switch (splitted[0]) {
                case "-1":
                    resultado[0]++;
                    break;
                case "0":
                    resultado[1]++;
                    break;
                default:
                    resultado[2]++;
                    break;
            }
        }
        return resultado;

    }

}
