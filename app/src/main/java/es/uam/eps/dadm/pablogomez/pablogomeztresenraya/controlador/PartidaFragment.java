/**
 * Fragmento que se encarga de mostrar el tablero de juego, con las fichas posicionadas
 * y dos miniaturas que muestran el tipo de ficha que utiliza cada jugador
 *
 * @author  Pablo Gomez Delgado
 * @version 1.5
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Dimension;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Movimiento3EnRaya;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Tablero3EnRaya;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.FichaView;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.Tablero3EnRayaView;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.TipoJuego;
import es.uam.eps.multij.AccionMover;
import es.uam.eps.multij.Evento;
import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Jugador;
import es.uam.eps.multij.JugadorAleatorio;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Partida;
import es.uam.eps.multij.Tablero;

public class PartidaFragment extends Fragment implements Jugador {

    private Tablero3EnRayaView tableroView;
    private TextView tv;
    private FichaView fvficha1;
    private FichaView fvficha2;
    private Tablero3EnRaya tablero;
    private Partida partida;
    private static boolean banderaSalir = false;

    /* Atributos para el control del tiempo */
    private Chronometer cronometro;
    private int tiempoPartida;

    /* Logica del juego */
    private ArrayList<Jugador> jugadores;

    public PartidaFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vista = inflater.inflate(R.layout.fragment_partida, container, false);

        if(cronometro != null){
            long basetime = cronometro.getBase();
            cronometro = (Chronometer) vista.findViewById(R.id.cronometro);
            cronometro.setBase(SystemClock.elapsedRealtime() + tiempoPartida*1000*10000);
        }
        else
            cronometro = (Chronometer) vista.findViewById(R.id.cronometro);

        if(banderaSalir)
            stopChronometer();
        else {
            cronometro.start();
            banderaSalir=false;
        }
        /* Vista del tablero */
        tableroView = (Tablero3EnRayaView) vista.findViewById(R.id.juego);
        tv = (TextView) vista.findViewById(R.id.infoCasilla);
        fvficha1 = (FichaView) vista.findViewById(R.id.ficha1);
        fvficha2 = (FichaView) vista.findViewById(R.id.ficha2);

        JugadorAleatorio jugadorAleatorio = new JugadorAleatorio("Aleatorio");

        jugadores = new ArrayList<>();
        jugadores.add(this);
        jugadores.add(jugadorAleatorio);

        tablero = new Tablero3EnRaya();

        if(PrincipalActivity.continuar) {
            this.stringToTablero(getActivity(), PrincipalActivity.getTableroGuardado(getActivity()));
        }
        tableroView.setTablero(tablero);

        /* Recogemos la seleccion del tipo de juego de las preferencias */
        String tipoJuego = PrincipalActivity.getEleccionFicha(getActivity());

        int FICHA1 = 0;
        fvficha1.setFicha(FICHA1);
        int FICHA2 = 1;
        fvficha2.setFicha(FICHA2);

        if (tipoJuego.equals("0")) {
            tableroView.setTipo(TipoJuego.SIMPLE);

            fvficha1.setTipo(TipoJuego.SIMPLE);
            fvficha2.setTipo(TipoJuego.SIMPLE);

        } else if (tipoJuego.equals("1")) {

            tableroView.setTipo(TipoJuego.STARWARS);
            fvficha1.setTipo(TipoJuego.STARWARS);
            fvficha2.setTipo(TipoJuego.STARWARS);

        }

        partida = new Partida(tablero, jugadores);
        partida.comenzar();
        tableroView.setOnCasillaSeleccionadaListener(new OnCasillaSeleccionadaListener() {
            @Override
            public void onCasillaSeleccionada(int fila, int columna) {

                if (tablero.getTurno() == jugadores.indexOf(PartidaFragment.this) && tablero.getEstado() == Tablero.EN_CURSO) {
                    Movimiento mov = new Movimiento3EnRaya(new Dimension(fila, columna));
                    try {
                        partida.realizaAccion(new AccionMover(PartidaFragment.this, mov));
                    } catch (ExcepcionJuego e) {
                        Toast.makeText(getActivity().getApplicationContext(), getResources().getString(R.string.errormov), Toast.LENGTH_SHORT).show();
                    }
                }
                if (comprobarFin()) {
                    configuraAlert(tablero.getEstado());
                }
            }

        });
        return vista;
    }

    @Override
    public String getNombre() {
        return PrincipalActivity.getNombreJugador(getActivity());
    }

    @Override
    public boolean puedeJugar(Tablero tablero) {
        return true;
    }


    /**
     * Metodo que comprueba que un juego ha finalizado
     *
     * @return boolean que indica si ha finalizado o no
     */
    private boolean comprobarFin() {
        return (tablero.getEstado() == Tablero.FINALIZADA || tablero.getEstado() == Tablero.TABLAS);
    }

    /**
     * Metodo que configura un alert dialog para mostrar
     *
     * @param finalizada el estado del tablero
     */
    private void configuraAlert(int finalizada) {


        AlertDialog.Builder alerta = new AlertDialog.Builder(getActivity());

        /* Creamos un textView para personalizar algo mas el alert dialog */
        TextView customView = new TextView(getActivity().getApplicationContext());
        customView.setBackgroundResource(R.drawable.textalert);
        customView.setTextColor(Color.WHITE);
        customView.setText(getResources().getString(R.string.finalizada));

        alerta.setCustomTitle(customView);
        alerta.setCancelable(false);

        /* Si la partida ha finalizado con algun resultado */
        if (finalizada == Tablero.FINALIZADA) {
            /* Si es tu turno */
            if (tablero.getTurno() == jugadores.indexOf(this)) {
                alerta.setMessage(R.string.fin_partida_ganas);
                agregarResultado(1);
            }
            else {
                alerta.setMessage(R.string.fin_partida_pierdes);
                agregarResultado(-1);
            }
        } else {
            alerta.setMessage(R.string.tablas);
            agregarResultado(0);
        }

        alerta.setPositiveButton(getResources().getString(R.string.salir), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        alerta.setNegativeButton(getResources().getString(R.string.reiniciar), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                cronometro.start();
                reiniciar();
            }
        });

        alerta.show();
    }

    /**
     * Metodo que se lanza cada vez que se produce un cambio en la partida
     *
     * @param evento El evento que ha probocado el cambio
     */
    @Override
    public void onCambioEnPartida(Evento evento) {

        switch (evento.getTipo()) {
            case Evento.EVENTO_CAMBIO:
                tableroView.invalidate();
                break;

            case Evento.EVENTO_CONFIRMA:
                break;

            case Evento.EVENTO_TURNO:

                Movimiento3EnRaya mov = (Movimiento3EnRaya) tablero.getUltimoMovimiento();
                StringBuilder texto = new StringBuilder(getResources().getString(R.string.ultimo_mov));
                texto.append(" ");

                if (mov.getCasilla().getCoordenadas().getX() == -1) {

                    texto.append(getResources().getString(R.string.inexistente));
                    tv.setText(texto);

                } else {

                    texto.append(mov.toString());
                    tv.setText(texto);
                }
                Animation animacionText = AnimationUtils.loadAnimation(getActivity(), R.anim.animtext);
                tv.startAnimation(animacionText);
                break;

            case Evento.EVENTO_FIN:
                fvficha1.invalidate();
                fvficha2.invalidate();
                tableroView.invalidate();
                stopChronometer();
                break;
        }
    }

    /**
     * Metodo que se encarga de reiniciar la vista del tablero de juego usando el metodo reset
     * del tablero, a demas de configurar sonidos y el tipo de ficha para la siguiente partida.
     */
    public void reiniciar() {

/*        if(tablero.getEstado() != Tablero.FINALIZADA){
            final AlertDialog.Builder guardar = new AlertDialog.Builder(getActivity());

            *//* Creamos un textView para personalizar algo mas el alert dialog *//*
            TextView customView = new TextView(getActivity().getApplicationContext());
            customView.setBackgroundResource(R.drawable.textalert);
            customView.setTextColor(Color.WHITE);
            customView.setText(getResources().getString(R.string.tituloguardar));

            stopChronometer();
            guardar.setCustomTitle(customView);
            guardar.setCancelable(false);
            guardar.setMessage(R.string.guardarpreg);
            guardar.setPositiveButton(R.string.si, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    PrincipalActivity.setTableroGuardado(getActivity(), tableroToString());
                    cronometro.start();
                    dialog.cancel();
                }
            });
            guardar.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cronometro.start();
                    PrincipalActivity.setTableroGuardado(getActivity(), PrincipalActivity.TABLERO_DEFAULT);
                    dialog.cancel();
                }
            });
            guardar.show();
        }*/
        banderaSalir=false;
        tablero.reset();
        cronometro.setBase(SystemClock.elapsedRealtime());
        StringBuilder texto = new StringBuilder(getResources().getString(R.string.ultimo_mov));
        texto.append(" ");
        texto.append(getResources().getString(R.string.inexistente));
        tv.setText(texto);
        if (PrincipalActivity.getValorEfectos(getActivity())) {
            if (tableroView.getTipo() == TipoJuego.SIMPLE) {
                MediaPlayer mp = MediaPlayer.create(getActivity().getApplicationContext(), R.raw.comienzo_android);
                mp.start();
            } else {
                MediaPlayer mp = MediaPlayer.create(getActivity(), R.raw.inici_saber);
                mp.start();
            }
        }
        partida.comenzar();
    }

    /**
     * Sobreescritura para restaurar una partida
     *
     * @param savedInstanceState Bundle a restaurar
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState == null)
            return;
        String tableroString = savedInstanceState.getString("TABLERO");
        String posTipo = savedInstanceState.getString("TIPO");
        Long tiempoEnMilis = (long) (savedInstanceState.getInt("CRONO") * 1000);
        cronometro.setBase(SystemClock.elapsedRealtime() - tiempoEnMilis);
        /* Recuperamos el tablero del string salvado */
        try {
            tablero.stringToTablero(tableroString);
        } catch (ExcepcionJuego excepcionJuego) {
            Toast.makeText(getActivity().getApplicationContext(),
                    getResources().getString(R.string.errorcarga), Toast.LENGTH_SHORT).show();
        }
        if (comprobarFin())
            configuraAlert(tablero.getEstado());

        if (posTipo != null) {
            switch (posTipo) {

                case "0":
                    Log.i("TIPO", "Entro en case 0 y cambio a SIMPLE");
                    PrincipalActivity.setEleccionFicha(getActivity(), TipoJuego.SIMPLE);
                    fvficha1.setTipo(TipoJuego.SIMPLE);
                    fvficha2.setTipo(TipoJuego.SIMPLE);
                    tableroView.setTipo(TipoJuego.SIMPLE);
                    break;

                case "1":
                    Log.i("TIPO", "Entro en case 1 y cambio a STAR WARS");
                    PrincipalActivity.setEleccionFicha(getActivity(), TipoJuego.STARWARS);
                    tableroView.setTipo(TipoJuego.STARWARS);
                    fvficha1.setTipo(TipoJuego.STARWARS);
                    fvficha2.setTipo(TipoJuego.STARWARS);
                    break;
            }
        }
        else{
            Log.i("TIPO", "Entro en case 0 y cambio a SIMPLE");
            PrincipalActivity.setEleccionFicha(getActivity(), TipoJuego.SIMPLE);
            fvficha1.setTipo(TipoJuego.SIMPLE);
            fvficha2.setTipo(TipoJuego.SIMPLE);
            tableroView.setTipo(TipoJuego.SIMPLE);
        }
        partida.continuar();

    }

    /**
     * Sobreescritura del metodo para salvar el estado
     *
     * @param outState Bundle a guardar
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {

        if(tablero != null) {
            outState.putString("TABLERO", tablero.tableroToString());
            String posTipo = tableroView.getTipo().toString();
            outState.putString("TIPO", posTipo);
            outState.putInt("CRONO", tiempoPartida);
        }
        super.onSaveInstanceState(outState);
    }

    /**
     * Metodo que realiza el paro y la estraccion del tiempo del cronometro
     */
    private void stopChronometer() {
        cronometro.stop();

        String chronometerText = cronometro.getText().toString();
        String array[] = chronometerText.split(":");
        if (array.length == 2) {
            tiempoPartida = Integer.parseInt(array[0]) * 60 +
                    Integer.parseInt(array[1]);
        } else if (array.length == 3) {
            tiempoPartida = Integer.parseInt(array[0]) * 60 * 60 +
                    Integer.parseInt(array[1]) * 60 + Integer.parseInt(array[2]);
        }
        Log.i("CRONO", "Tiempo en segs: " + String.valueOf(tiempoPartida));
    }

    /**
     * Metodo que hace un start del cronometro
     * No tiene modificador de visibilidad para que solo sea accesible desde la clase
     * y el package
     */
    void startCrhonometer(){
        cronometro.start();
    }
    /**
     * ganar|nombre|tablero
     */
    private void agregarResultado(int ganada){

        Repository db = RepositoryFactory.createRepository(getActivity());
        if(db == null){
            return;
        }
        String uuid = PrincipalActivity.getPlayerUUID(getActivity());
        String jugadorcontrario = (jugadores.indexOf(PartidaFragment.this) != 0) ?
                                    jugadores.get(0).getNombre() : jugadores.get(1).getNombre();

        int segundos = tiempoPartida;
        int puntos=0;

        String otherinfo = String.valueOf(ganada) + "|" + jugadorcontrario + "|" + tablero.tableroToString();
        Repository.BooleanCallback callback = new Repository.BooleanCallback() {
            @Override
            public void onResponse(boolean ok) {
                if(!ok)
                    Toast.makeText(getActivity(), "No se ha podido añadir el resultado a la db", Toast.LENGTH_SHORT).show();
            }
        };

        db.addResult(uuid, segundos, puntos, otherinfo, callback);

    }

    /**
     * Metodo que introduce un string al tablero actual
     * @param tablero String con el tablero a introducir
     */
    public void stringToTablero(Context context, String tablero) {
        try {
            String splitted[];
            splitted = tablero.split("\\|");


            /** Actualizamos el tiempo del cronometro, tanto el base como el texto */
            Long tiempoEnMilis = Long.parseLong(splitted[0]) * 1000;
            cronometro.setBase(SystemClock.elapsedRealtime() - tiempoEnMilis);
            SimpleDateFormat sdf = new SimpleDateFormat("mm:ss", Locale.US);
            String texto = sdf.format(tiempoEnMilis);
            cronometro.setText(texto);

            /** Actualizamos el tablero */
            this.tablero.stringToTablero(splitted[1]);
            cronometro.start();
        } catch (ExcepcionJuego excepcionJuego) {
            excepcionJuego.printStackTrace();
        }
    }

    /**
     * Metodo que devuelve el tableroToString de tablero
     * @return El tablero en forma de string
     */
    public String tableroToString(){
        stopChronometer();
        return String.valueOf(tiempoPartida) + "|" + tablero.tableroToString();
    }
    /**
     * Metodo que devuelve el estado del tablero desde el fragmento
     * @return El estado del tablero
     */
    public int geEstadoTablero(){
        return tablero.getEstado();
    }

    public TipoJuego getEstadoView(){
        return tableroView.getTipo();
    }
    /**
     * Devuelve si el tablero esta vacio
     * @return True si el tablero esta vacio, false en caso contrario
     */
    public boolean isTableroVacio(){
        return tablero.getNumJugadas() == 0;
    }


    public void cambioFichas(){


        if (tableroView.getTipo() == TipoJuego.SIMPLE) {
            Log.i("TIPO", "Entro en action_fichas y cambio a STAR WARS");
            tableroView.setTipo(TipoJuego.STARWARS);

            PrincipalActivity.setEleccionFicha(getActivity(), TipoJuego.STARWARS);
            /* Cambio e invalidacion de la vista de las fichas */
            fvficha1.setTipo(TipoJuego.STARWARS);
            fvficha2.setTipo(TipoJuego.STARWARS);
            fvficha1.invalidate();
            fvficha2.invalidate();
            reiniciar();

        } else {
            Log.i("TIPO", "Entro en action_fichas y cambio a SIMPLE");
            tableroView.setTipo(TipoJuego.SIMPLE);

            PrincipalActivity.setEleccionFicha(getActivity(), TipoJuego.SIMPLE);
            /* Cambio e invalidacion de la vista de las fichas */
            fvficha1.setTipo(TipoJuego.SIMPLE);
            fvficha2.setTipo(TipoJuego.SIMPLE);
            fvficha1.invalidate();
            fvficha2.invalidate();
            reiniciar();
        }
    }
}
