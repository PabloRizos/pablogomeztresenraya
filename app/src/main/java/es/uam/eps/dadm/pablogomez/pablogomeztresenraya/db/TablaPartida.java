package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;
/**
 * Clase que encapsula los campos de la tabla de Partidas de la base de datos
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   02-04-2016
 */
public class TablaPartida {

        // Index of player
        public static final String ID = "id";

        // Index of round
        public static final String PARTIDA = "partida";

        // Duration of round
        public static final String DURACION = "duracion";

        // Number of pieces left on the board
        public static final String NPIEZAS = "npiezas";

        // Date of the round
        public static final String FECHA = "fecha";

        // Name of the table
        static final String TABLE_NAME = "rounds";

        //Estado de la partida (partidaToString)
        static final String ESTADO_PARTIDA = "estado";
}
