/**
 * Interfaz que declara un listener para recoger la fila y la columna de la casilla
 * seleccionada
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


public interface OnCasillaSeleccionadaListener {

    void onCasillaSeleccionada(int fila, int columna);

}
