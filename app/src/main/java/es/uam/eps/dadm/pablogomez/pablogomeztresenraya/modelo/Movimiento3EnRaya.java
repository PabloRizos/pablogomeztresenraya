package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;

import es.uam.eps.multij.Movimiento;

/**
 * Representa un movimiento del juego 3 en raya
 */
public class Movimiento3EnRaya extends Movimiento {

	/** Casilla a la que se quiere mover */
	private Casilla casilla;

	/**
	 * Dos constructores, con sobrecarga para poder inicializar dependiendo de
	 * la situacion
	 */
	public Movimiento3EnRaya(Dimension coordenadas, Ficha ficha) {
		this.casilla = new Casilla(coordenadas, ficha);
	}

	public Movimiento3EnRaya(Dimension coordenadas) {
		this.casilla = new Casilla(coordenadas);
	}

	/** toString que llama solo al toString de coordenadas */
	@Override
	public String toString() {
		return this.getCasilla().getCoordenadas().toString();
	}

	/** Equals comprueba que el movimiento 3 en raya sea igual a otro */
	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Movimiento3EnRaya))
			return false;
		if (((Movimiento3EnRaya) o).getCasilla().getCoordenadas().equals(
				this.getCasilla().getCoordenadas()))
			return true;
		return false;
	}

	/** Getter de casilla */
	public Casilla getCasilla() {
		return casilla;
	}

}
