package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.PrincipalActivity;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Tablero3EnRaya;
import es.uam.eps.multij.ExcepcionJuego;

public class MiArrayAdapter extends ArrayAdapter<Repository.Result>{


    private class Holder {
        TextView textView;
        ImageView imageView;
        Tablero3EnRayaView tableroView;
        Tablero3EnRaya tablero;
    }

    public MiArrayAdapter(Context context) {
        super(context, R.layout.mi_list_item);
    }

    /**
     * Versión más eficiente. Evita también el uso de findViewById guardando una referencia directa
     * a los elementos del layout fila que necesitamos modificar. Este método puede ser entorno a un
     * 15% más rápido que el anterior.
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        View vistaLinea;
        Holder holder;

        vistaLinea = convertView;
        if ( vistaLinea == null ) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vistaLinea = inflater.inflate(R.layout.mi_list_item, parent, false);
            // Guardamos en esta clase la referencia directa al TextView e ImageView
            holder = new Holder();
            holder.imageView = (ImageView) vistaLinea.findViewById(R.id.imgGanar);
            holder.textView  = (TextView) vistaLinea.findViewById(R.id.textGanar);
            holder.tableroView = (Tablero3EnRayaView) vistaLinea.findViewById(R.id.tableroview);
            // El holder se guarda en el Tag de la Vista, que sirve para almacenar un Object
            vistaLinea.setTag(holder);
        }

        // Recuperamos el holder de las filas ya creadas
        holder = (Holder)vistaLinea.getTag();
        String resultados[] = getItem(position).otherInfo.split("\\|");

        String jugadores = PrincipalActivity.getNombreJugador(getContext()) + " VS " + resultados[1];
        holder.textView.setText(jugadores);

        /** Nos aseguramos la economizacion de recursos */
        if(holder.tablero == null)
            holder.tablero = new Tablero3EnRaya();

        try {
            holder.tablero.stringToTablero(resultados[2]);
        } catch (ExcepcionJuego excepcionJuego) {
            excepcionJuego.printStackTrace();
        }
        holder.tableroView.setTablero(holder.tablero);

        switch (resultados[0]) {
            case "1":
                vistaLinea.setBackgroundResource(R.drawable.milist_ganar);
                holder.imageView.setImageResource(R.drawable.ic_win_dedo);
                break;
            case "-1":
                vistaLinea.setBackgroundResource(R.drawable.milist_perder);
                holder.imageView.setImageResource(R.drawable.ic_lose_dedo);
                break;
            default:
                vistaLinea.setBackgroundResource(R.drawable.milist_empatar);
                holder.imageView.setImageResource(R.drawable.ic_empate);
                break;
        }
        return vistaLinea;
    }
}

