/**
 * Actividad donde se realizará la creación de una cuenta de usuario
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   30-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Conexion;

public class CuentaActivity extends AppCompatActivity{

    private EditText nombreText;
    private EditText passwordText;
    private EditText getPasswordText2;
    private Button botonCuenta;
    private Repository db;
    private AlertDialog ad;
    private final static int MIN_PASS = 4;
    private Switch online;

    /**
     * Metodo que maneja todos los parametros de la clase
     * @param savedInstanceState Bundle salvado anteriormente
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cuenta);

        nombreText = (EditText) findViewById(R.id.nombrelogin);
        passwordText = (EditText) findViewById(R.id.passcuenta);
        getPasswordText2 = (EditText) findViewById(R.id.passcuenta2);
        botonCuenta = (Button) findViewById(R.id.botoncuenta);

        /* Metodo que redirige a la creacion de la cuenta */
        botonCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Conexion conn = new Conexion((ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE));
                online = (Switch) findViewById(R.id.onlineswitch);

                String nombre = nombreText.getText().toString();
                String password = passwordText.getText().toString();
                String password2 = getPasswordText2.getText().toString();

                boolean conexion = true;
                /** Llamada y funcionalidad de la base de datos */
                if (online != null) {
                    if (online.isChecked() && conn.compruebaConexion()) {
                        PrincipalActivity.setLocalState(CuentaActivity.this, false);
                    }
                    /** Si el switch esta checkeado y no hay conexion */
                    else if (online.isChecked() && !conn.compruebaConexion()) {
                        Toast.makeText(CuentaActivity.this, "ERROR: No hay conexion a internet", Toast.LENGTH_SHORT).show();
                        PrincipalActivity.setLocalState(CuentaActivity.this, true);
                        conexion = false;
                    }
                    /** Si el switch no esta chequeado */
                    else {
                        PrincipalActivity.setLocalState(CuentaActivity.this, true);
                    }
                }
                /** No hay switch */
                else {
                    PrincipalActivity.setLocalState(CuentaActivity.this, true);
                }

                /** Recuperamos la conexion con la base de datos y si no existe la creamos */
                db = RepositoryFactory.createRepository(CuentaActivity.this);

                if (conexion) {
                    /** Comprobacion de errores en la introduccion de texto del usuario */
                    if (nombre.isEmpty() || password.isEmpty() || password2.isEmpty())
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.errorMissing), Toast.LENGTH_SHORT).show();
                    else if (password.length() < 4)
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.errorPass), Toast.LENGTH_SHORT).show();
                    else if (!password.equals(password2))
                        Toast.makeText(getApplicationContext(),
                                getResources().getString(R.string.errorEquals), Toast.LENGTH_SHORT).show();
                    else {

                        Repository.LoginRegisterCallback callback = new Repository.LoginRegisterCallback() {

                            @Override
                            public void onLogin(String playerUuid) {

                                Toast.makeText(CuentaActivity.this, getResources().getString(R.string.creadousuexito) + " con ID: " + playerUuid, Toast.LENGTH_SHORT).show();
                                Log.i("DEBUG", "onLogin UUID: " + playerUuid);
                                Intent intento = new Intent(CuentaActivity.this, LoginActivity.class);
                                startActivity(intento);

                            }

                            @Override
                            public void onError(String error) {
                                ad = new AlertDialog.Builder(CuentaActivity.this)
                                        .setTitle(R.string.error_login)
                                        .setMessage(error)
                                        .setNeutralButton(R.string.ok,
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        ad.cancel();
                                                    }
                                                }).show();
                                nombreText.setText("");
                                passwordText.setText("");
                                getPasswordText2.setText("");
                                Log.e("DEBUG", "Error en callback: " + error);
                            }
                        };
                        /** Ejecutamos la operacion en la base de datos */
                        db.register(nombre, password, callback);
                    }
                }
            }
        });
    }
}
