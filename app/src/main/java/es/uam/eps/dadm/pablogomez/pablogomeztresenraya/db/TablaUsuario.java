package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;
/**
 * Clase que encapsula los campos de la tabla de Usuarios de la base de datos
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   02-04-2016
 */
public class TablaUsuario {
        // Unique id of each player
        public static final String UUID = "id";

        // Name of the player
        public static final String USERNAME = "username";

        // Password of the player
        public static final String PASSWORD = "password";

        // Name of the table
        static final String TABLE_NAME = "users";
}
