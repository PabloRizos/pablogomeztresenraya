package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;


/**
 * Clase para comprobar la conexion
 */
public class Conexion{
    ConnectivityManager connMgr;

    public Conexion(ConnectivityManager connMgr){
        this.connMgr = connMgr;
    }



    /**
     * Metodo que comprueba la conexion a internet
     * @return Booleano indicanco si hay o no conexion a internet (true o false)
     */
    public boolean compruebaConexion(){
        boolean wifiConnected = false;
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if(networkInfo != null)
            wifiConnected = networkInfo.isConnected();

        return wifiConnected;
    }

//
//    public boolean conexionHandler(Context context){
//
//        boolean conexion = true;
//        Conexion conn = new Conexion((ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE));
//        /** Llamada y funcionalidad de la base de datos */
//        if (online != null) {
//            if(online.isChecked() && conn.compruebaConexion()) {
//                PrincipalActivity.setLocalState(LoginActivity.this, false);
//            }
//            /** Si el switch esta checkeado y no hay conexion */
//            else if(online.isChecked() && !conn.compruebaConexion()){
//                PrincipalActivity.setLocalState(LoginActivity.this, true);
//                conexion = false;
//            }
//            /** Si el switch no esta chequeado */
//            else{
//                PrincipalActivity.setLocalState(LoginActivity.this, true);
//            }
//        }
//        /** No hay switch */
//        else{
//            PrincipalActivity.setLocalState(LoginActivity.this, true);
//        }
//        return conexion
//    }


}
