package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

/**
 * Clase que implementa la funcionalidad basica de la Base de Datos
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   02-04-2016
 */
import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class MiDataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "tresenraya.db";
    private static final int DATABASE_VERSION = 1;


    public MiDataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Metodo que se ejecuta al crearse una instancia de nuestra base de datos
     * @param db La base de datos a crear
     */
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    /**
     * Si la base de datos es actualizada, se ejecuta este metodo para comprobar su consistencia
     * @param db la base de datos a actualizar
     * @param oldVersion La version anterior de la base de datos
     * @param newVersion La nueva version de la base de datos
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TablaUsuario.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + TablaPartida.TABLE_NAME);
        createTable(db);
    }

    /**
     * Metodo que crea las tablas de mi base de datos
     * @param db La base de datos donde crear las tablas
     */
    private void createTable(SQLiteDatabase db) {
        String str1 = "CREATE TABLE " + TablaUsuario.TABLE_NAME + " ("
                + TablaUsuario.UUID + " TEXT PRIMARY KEY, "
                + TablaUsuario.USERNAME + " TEXT UNIQUE, "
                + TablaUsuario.PASSWORD + " TEXT);";

        String str2 = "CREATE TABLE " + TablaPartida.TABLE_NAME
                + " (" + TablaPartida.ID
                + " TEXT REFERENCES " + TablaUsuario.TABLE_NAME + ", "
                + TablaPartida.PARTIDA + " INTEGER, "
                + TablaPartida.DURACION + " INTEGER, "
                + TablaPartida.NPIEZAS + " INTEGER, "
                + TablaPartida.FECHA + " DATE,"
                + TablaPartida.ESTADO_PARTIDA + " TEXT, "
                + "PRIMARY KEY (" + TablaPartida.ID + ", " + TablaPartida.PARTIDA + "));";
        try {
            db.execSQL(str1);
            db.execSQL(str2);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


}
