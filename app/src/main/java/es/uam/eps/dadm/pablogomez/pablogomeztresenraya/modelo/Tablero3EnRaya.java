package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;

import java.util.ArrayList;

import es.uam.eps.multij.ExcepcionJuego;
import es.uam.eps.multij.Movimiento;
import es.uam.eps.multij.Tablero;

/**
 * Esta clase representa un tablero para el juego de 3 en raya
 */
public class Tablero3EnRaya extends Tablero {

	/** Minimo de jugadas para finalizar la partida */
	public static final int MIN_JUGADAS_FIN = 4;

	/** Numero de filas del tres en raya */
	public static final int NUM_FILAS = 3;

	/** Numero de columnas del tres en raya */
	public static final int NUM_COLUMNAS = 3;

	/** Representacion del tablero con un ArrayList de ArrayLists */
	private ArrayList<ArrayList<Casilla>> tablero;

	/**
	 * Constructor del tablero con incializacion del mismo
	 * */
	public Tablero3EnRaya() {
		super();
		this.tablero = new ArrayList<ArrayList<Casilla>>();
		this.inicializaTablero3EnRaya();
	}

	/**
	 * Inicializa el tablero, con todas sus casillas a un tablero con fichas del
	 * Tipo.VACIO
	 * 
	 * */
	private void inicializaTablero3EnRaya() {

		/** Inicializamos el tablero */
		ArrayList<Casilla> columna;
		for (int i = 0; i < NUM_FILAS; i++) {
			columna = new ArrayList<>();
			for (int j = 0; j < NUM_COLUMNAS; j++) {
				columna.add(j, new Casilla(new Dimension(i, j)));
			}
			this.tablero.add(i, columna);
		}
		/** Inicializacion de atributos de la clase */
		this.estado = EN_CURSO;
		this.numJugadas = 0;
		this.numJugadores = 2;
		this.turno = 0;

		/** Si ult. movimiento es (-1, -1) significa que no se ha movido aun */
		this.ultimoMovimiento = new Movimiento3EnRaya(new Dimension(-1, -1));
	}

	/**
	 * Metodo que mueve una ficha en un tablero
	 * 
	 * */
	@Override
	protected void mueve(Movimiento m) throws ExcepcionJuego {
		if (m == null)
			throw new ExcepcionJuego(
					"ERROR. Tablero3EnRaya.mueve(): Movimiento no generado correctamente");
		if (!esValido(m))
			throw new ExcepcionJuego(
					"ERROR. Tablero3EnRaya.mueve(): Movimiento no permitido");
		else {
			/** Recogemos la info de la casilla a mover */
			Dimension coordenadas = ((Movimiento3EnRaya) m).getCasilla()
					.getCoordenadas();
			Ficha t = this.getFichaTurno();

			/** Se "efectua" el movimiento */
			this.tablero.get(coordenadas.getX()).get(coordenadas.getY())
					.setFicha(t);

		}

		this.ultimoMovimiento = m;

		/** Comprobamos si estamos en TABLAS, EN_CURSO, o FINALIZADA */
		this.estado = this.compruebaTablero();

		if (!(this.estado == TABLAS || this.getEstado() == FINALIZADA))
			this.cambiaTurno();
		else
			this.numJugadas++;

	}

	/**
	 * Metodo que comprueba el tablero del juego y devuelve la constante
	 * correspondiente al estado
	 */
	private int compruebaTablero() {

		/**
		 * Si el numero de jugadas no es menor que el minimo de jugadas
		 * para poder ganar, continuamos.
		 */
		if (this.numJugadas + 1 < MIN_JUGADAS_FIN)
			return EN_CURSO;

		/** Recogemos las variables a usar en este metodo */
		Movimiento3EnRaya m = (Movimiento3EnRaya) this.ultimoMovimiento;
		Integer ultCol = m.getCasilla().getCoordenadas().getX();
		Integer ultFila = m.getCasilla().getCoordenadas().getY();
		Ficha f = this.getFichaTurno();
		int contFichas = 0;

		/** Bucle que comprueba si existen 3 fichas seguidas en la columna */
		for (int x = 0; x < this.tablero.size(); x++) {
			if (this.tablero.get(x).get(ultFila).getFicha().equals(f))
				contFichas++;
		}

		/** Comprobacion de finalizacion de partida */
		if (contFichas >= 3)
			return FINALIZADA;
		else
			contFichas = 0;

		/** Bucle que comprueba si existen 3 fichas seguidas en la fila */
		for (int y = 0; y < this.tablero.get(ultCol).size(); y++) {
			if (this.tablero.get(ultCol).get(y).getFicha().equals(f))
				contFichas++;
		}

		/** Comprobacion de finalizacion de partida */
		if (contFichas >= 3)
			return FINALIZADA;

		/**
		 * Obtenemos la ficha del centro del tablero para la posterior
		 * comprobacion
		 */
		Ficha centro = this.tablero.get(1).get(1).getFicha();
		
		/** Comprobacion de las dos diagonales del tablero */
		if ((this.tablero.get(0).get(0).getFicha().equals(f)
				&& centro.equals(f) && this.tablero.get(2).get(2).getFicha()
				.equals(f))
				|| (this.tablero.get(0).get(2).getFicha().equals(f)
						&& centro.equals(f) && this.tablero.get(2).get(0)
						.getFicha().equals(f)))
			return FINALIZADA;

		/**
		 * Despues de la comprobacion de un ganador, comprobacion de partida en
		 * tablas
		 */
		if (this.movimientosValidos().isEmpty())
			return TABLAS;

		return EN_CURSO;
	}

	/** Metodo que comprueba todos los movimientos validos */
	@Override
	public boolean esValido(Movimiento m) {
        return this.movimientosValidos().contains(m);
	}

	/** Metodo que crea un array de movimientos validos */
	@Override
	public ArrayList<Movimiento> movimientosValidos() {

		/** El Array de movimientos a devolver */
		ArrayList<Movimiento> movimientos = new ArrayList<>();

		/**
		 * Sacamos el array correspondiente a la clave i del primer array y
		 * comprobamos si la casilla no se encuentra asignada
		 * */
		for (int i = 0; i < this.tablero.size(); i++) {
			for (Casilla col : this.tablero.get(i))
				if (col.getFicha().equals(Ficha.VACIO))
					movimientos
							.add(new Movimiento3EnRaya(col.getCoordenadas()));

		}
		return movimientos;
	}

	/**
	 * Metodo que devuelve un string del tipo:
	 * '1,0,2,0,1,1,1, , ,X, , ,O,O,X, '
	 * 
	 * @return
	 *         Devuelve la cadena a partir del tablero actual
	 */
	@Override
	public String tableroToString() {

		/** Usamos StringBuilder por temas de aprovechamiento de memoria */
		StringBuilder sb = new StringBuilder();

		/** Si no hay ul. movimiento, se guarda un (-1,-1) */
		int x = ((Movimiento3EnRaya) this.ultimoMovimiento).getCasilla()
				.getCoordenadas().getX();
		int y = ((Movimiento3EnRaya) this.ultimoMovimiento).getCasilla()
				.getCoordenadas().getY();
		String tablero = this.estado + "," + this.numJugadas + "," + this.numJugadores
				+ "," + this.turno + "," + x + "," + y + ",";
		sb.append(tablero);

		/** Se recorre el tablero */
		for (ArrayList<Casilla> col : this.tablero) {
			for (Casilla cas : col) {
				tablero = cas.toString() + ",";
				sb.append(tablero);
			}
		}
		return sb.toString();
	}

	/**
	 * Metodo que recoge un string y crea un tablero a partir de el
	 * 
	 * @param cadena
	 *            Una cadena que represente el tablero a recoger.
	 * */
	@Override
	public void stringToTablero(String cadena) throws ExcepcionJuego {
		if (cadena == null || cadena.isEmpty())
			throw new ExcepcionJuego(
					"ERROR. stringToTablero(): Tablero suministrado nulo");

		/**
		 * Parseamos la cadena recibida para poder almacenar la informacion en
		 * memoria
		 */

		/** Cadena resultante de partir cadena */
		String[] cadena2 = cadena.split(",");
		try {

			/** Recogida del estado */
			this.estado = Integer.parseInt(cadena2[0]);
			/** Numero de jugadas */
			this.numJugadas = Integer.parseInt(cadena2[1]);
			/** Numero de jugadores */
			this.numJugadores = Integer.parseInt(cadena2[2]);
			/** Turno */
			this.turno = Integer.parseInt(cadena2[3]);

			/** Parametros de Ultimo Movimiento */
			int x, y;

			/** Coordenada x */
			x = Integer.parseInt(cadena2[4]);
			/** Coordenada y */
			y = Integer.parseInt(cadena2[5]);

			/**
			 * Si las coordenadas indican que no hay ult. mov. paramos la
			 * ejecucion de la funcion
			 */
			Dimension ultMovCoord;
			if (x < 0 || y < 0) {
				ultMovCoord = new Dimension(-1, -1);
				this.ultimoMovimiento = new Movimiento3EnRaya(ultMovCoord);
				return;
			} else
				ultMovCoord = new Dimension(x, y);
				/** Si hay ultimo movimiento, lo igualamos */
				this.ultimoMovimiento = new Movimiento3EnRaya(ultMovCoord);

		} catch (NumberFormatException e) {

			/**
			 * Si tenemos una excepcion en parseInt(), la encapsulamos con
			 * ExcepcionJuego
			 */
			throw new ExcepcionJuego(
					"ERROR. stringToTablero(): Cabecera del tablero mal formada",
					e);
		}

		/** Bucles que recogen los componentes de los arrays */
		int posArray = 6;
		for (int i = 0; i < NUM_FILAS; i++) {
			for (int j = 0; j < NUM_COLUMNAS; j++) {
				switch (cadena2[posArray]) {
				case " ":
					// Si hay un blanco
					this.tablero.get(i).get(j).setFicha(Ficha.VACIO);
					posArray++;
					break;
				case "X":
					// Si tenemos el tipo 1
					this.tablero.get(i).get(j).setFicha(Ficha.TIPO1);
					posArray++;
					break;
				case "O":
					// Si tenemos el tipo 2
					this.tablero.get(i).get(j).setFicha(Ficha.TIPO2);
					posArray++;
					break;
				default:
					throw new ExcepcionJuego(
							"ERROR. stringToTablero(): Casilla del tablero desconocida");
				}

			}
		}
	}

	/**
	 * Metodo para mostrar el tablero por pantalla
	 * 
	 * @return String
	 *         Una cadena representativa del tablero actual para su impresión
	 *         por pantalla
	 * */
	@Override
	public String toString() {

		/** String builder para optimizar el uso de memoria */
		StringBuilder sb = new StringBuilder();
		sb.append("    |   0   |   1   |   2   |\n");
		sb.append("-----------------------------\n");

		int i = 0;
		/** Bucle de creacion del tablero */
		for (ArrayList<Casilla> col : this.tablero) {
			sb.append("    |       |       |       |\n");
			sb.append(" " + i + "  |   ");
			i++;
			for (Casilla cas : col) {

				sb.append(cas.toString() + "   |   ");

			}
			sb.append("\n    |       |       |       |\n");
			sb.append("-----------------------------\n");

		}
		sb.append("Numero de jugadas: " + this.numJugadas);
		return sb.toString();

	}

	/** Obtenemos el tipo de ficha que toca */
	private Ficha getFichaTurno() {

		return this.turno == 0 ? Ficha.TIPO1 : Ficha.TIPO2;
	}

	/**
	 * Pone el tablero en su posicion inicial
	 * 
	 * @return si se ha podido resetear el tablero.
	 */
	@Override
	public boolean reset() {

		/** Deja todas las casillas a Ficha.VACIO */
		for (ArrayList<Casilla> columna : this.tablero) {
			for (Casilla cas : columna) {
				cas.setFicha(Ficha.VACIO);
			}
		}
		this.turno = 0;
		this.estado = EN_CURSO;
		this.numJugadas = 0;
		numJugadas = 0;
		ultimoMovimiento = new Movimiento3EnRaya(new Dimension(-1, -1));
		return true;
	}
	/**
	 * Devuelve la ficha correspondiente a una posicion
	 *
	 * @return la ficha correspondiente a la casilla
	 */
	public Ficha getFichaPosicion(int x, int y){
		return tablero.get(x).get(y).getFicha();
	}
}
