package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.os.Bundle;
import android.app.Fragment;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreferenciasFragment extends PreferenceFragment {

        public void onCreate(Bundle savedInstanceState){
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.settings);
            this.findPreference(PrincipalActivity.KEY_EFECTOS).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener) getActivity());
            this.findPreference(PrincipalActivity.KEY_MUSICA).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener) getActivity());
            this.findPreference(PrincipalActivity.KEY_ELECCION_FICHA).setOnPreferenceChangeListener((Preference.OnPreferenceChangeListener) getActivity());

        }
}
