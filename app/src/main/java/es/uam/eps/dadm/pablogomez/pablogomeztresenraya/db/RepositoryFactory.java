package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db;

import android.content.Context;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.PrincipalActivity;

/**
 * @author gonzalo on 29/02/16.
 */
public class RepositoryFactory {

    private static Repository repository=null;

    public static Repository createRepository(Context context) {
            boolean local = PrincipalActivity.getLocalState(context);
            repository = local ? getInstanceLocal(context) : getInstanceOnline(context);
        try {
            repository.open();
        }
        catch (Exception e) {
            return null;
        }
        return repository;
    }

    /**
     * Devuelve la instancia del repositorio
     * @param context el contexto
     * @return El repositorio
     */
    private static Repository getInstanceOnline(Context context) {
        return MiServerDB.getInstance(context);
    }

    /**
     * Metodo que devuelve la instancia del repositorio creado
     * @param context Contexto de la aplicacion
     * @return Repositorio almacenado en esta clase
     */
    private static Repository getInstanceLocal(Context context){
        return new MiDB(context);
    }
}
