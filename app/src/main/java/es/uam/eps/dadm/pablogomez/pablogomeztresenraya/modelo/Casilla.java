package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;


public class Casilla {
	
	private Dimension coordenadas;
	private Ficha ficha;
	
	//Constructor para inicializar una casilla sin marcarla con un tipo determinado
	public Casilla(Dimension coordenadas){
		this.setCoordenadas(coordenadas);
		this.setFicha(Ficha.VACIO);
	}
	//Constructor para inicializar una casilla marcada con el tipo de un jugador
	public Casilla(Dimension coordenadas, Ficha ficha){
		this.setCoordenadas(coordenadas);
		this.setFicha(ficha);
	}
	public Ficha getFicha() {
		return ficha;
	}
	public void setFicha(Ficha ficha) {
		this.ficha = ficha;
	}
	public Dimension getCoordenadas() {
		return coordenadas;
	}
	public void setCoordenadas(Dimension coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	@Override
	public String toString(){
		
		return this.getFicha().toString();
		
	}

}
