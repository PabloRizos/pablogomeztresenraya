package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;

/**
 * Actividad donde se realizará el login de la app
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   30-03-2016
 */

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo.Conexion;

public class LoginActivity extends AppCompatActivity {

    private EditText nombreText;
    private EditText passwordText;
    private Repository db;
    private AlertDialog ad;
    private String nombre;
    private String password;
    private Button botonEnviar;
    private Button botonCuenta;
    private Switch online;

    /**
     * Metodo que maneja todos los parametros de la clase
     * @param savedInstanceState Bundle salvado anteriormente
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        online = (Switch) findViewById(R.id.onlineswitch);

        nombreText = (EditText) findViewById(R.id.nombrelogin);
        passwordText = (EditText) findViewById(R.id.passlogin);
        botonEnviar = (Button) findViewById(R.id.botonlogin);
        botonCuenta = (Button) findViewById(R.id.botoncuenta);

        String nombrePref = PrincipalActivity.getNombreJugador(getApplicationContext());

        String passPref = PrincipalActivity.getPasswordJugador(getApplicationContext());
        if(!nombrePref.equals(PrincipalActivity.NOMBRE_JUGADOR_DEFAULT) &&
                !passPref.equals(PrincipalActivity.PASS_JUGADOR_DEFAULT)){
            nombreText.setText(nombrePref);
            passwordText.setText(passPref);
        }

        /* Metodo que redirige a la actividad principal a traves del boton Enviar */
        botonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean conexion = true;

                Conexion conn = new Conexion((ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE));
                /** Llamada y funcionalidad de la base de datos */
                if (online != null) {
                    if (online.isChecked() && conn.compruebaConexion()) {
                        PrincipalActivity.setLocalState(LoginActivity.this, false);
                    }
                    /** Si el switch esta checkeado y no hay conexion */
                    else if (online.isChecked() && !conn.compruebaConexion()) {
                        Toast.makeText(LoginActivity.this, "ERROR: No hay conexion a internet", Toast.LENGTH_SHORT).show();
                        PrincipalActivity.setLocalState(LoginActivity.this, true);
                        conexion = false;
                    }
                    /** Si el switch no esta chequeado */
                    else {
                        PrincipalActivity.setLocalState(LoginActivity.this, true);
                    }
                }
                /** No hay switch */
                else {
                    PrincipalActivity.setLocalState(LoginActivity.this, true);
                }

                if (conexion) {
                    boolean local = PrincipalActivity.getLocalState(LoginActivity.this);
                    db = RepositoryFactory.createRepository(LoginActivity.this);

                    nombre = nombreText.getText().toString();
                    password = passwordText.getText().toString();

                    /** Seteamos el callback con sus métodos */
                    final Repository.LoginRegisterCallback callback = new Repository.LoginRegisterCallback() {
                        @Override
                        public void onLogin(String playerUuid) {

                            Intent intento = new Intent(LoginActivity.this, PrincipalActivity.class);
                            String uuid = PrincipalActivity.getPlayerUUID(getApplicationContext());
                            Log.i("DEBUG:", uuid);
                            if (uuid.equals("0"))
                                PrincipalActivity.setPlayerUUID(getApplicationContext(), playerUuid);
                            PrincipalActivity.setNombreJugador(getApplicationContext(), nombre);
                            PrincipalActivity.setPasswordJugador(getApplicationContext(), password);
                            startActivity(intento);
                        }

                        @Override
                        public void onError(String error) {
                            ad = new AlertDialog.Builder(LoginActivity.this)
                                    .setTitle(R.string.error_login)
                                    .setMessage(error)
                                    .setNeutralButton(R.string.ok,
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    ad.cancel();
                                                }
                                            }).show();
                            nombreText.setText("");
                            passwordText.setText("");
                        }
                    };

                    db.login(nombre, password, callback);
                }
            }
        });

        /* Metodo que redirige a la pantalla de nueva cuenta a traves del boton de cuenta */
        botonCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Intent intento = new Intent(LoginActivity.this, CuentaActivity.class);
                    startActivity(intento);
                }
        });
    }
}
