/**
 * Actividad donde se realizarán todos los cambios de fragmentos y tendrá lugar
 * la funcionalidad principal de la app
 *
 * @author  Pablo Gomez Delgado
 * @version 2.0
 * @since   25-03-2016
 */

package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.RepositoryFactory;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista.TipoJuego;
import es.uam.eps.multij.Tablero;


public class PrincipalActivity extends AppCompatActivity implements OnBotonSeleccionadoListener, Preference.OnPreferenceChangeListener {

    /**
     * Atributos de la clase
     */
    final static int MAXDIMEN = 600;
    private FragmentTransaction transaccion;
    private FragmentManager fmanager;
    private boolean tableta = false;
    private MediaPlayer musicaFondo;
    private Menu menu;
    private Repository db;


    /**
     * Valores de recogida y setteado de preferencias
     */
    public final static String KEY_EFECTOS = "efectos";
    public final static boolean EFECTOS_DEFAULT = true;
    public final static String KEY_MUSICA = "musica";
    public final static boolean MUSICA_DEFAULT = true;
    public final static String KEY_NOMBRE_JUGADOR = "nombreJugador";
    public final static String KEY_ELECCION_FICHA = "eleccionficha";
    public final static String NOMBRE_JUGADOR_DEFAULT = "jugador";
    public final static String ELECCION_FICHA_DEFAULT = "0";
    public final static String KEY_PASS_JUGADOR = "passJugador";
    public final static String PASS_JUGADOR_DEFAULT = "00000000";
    public static String KEY_PLAYER_UUID ="uuid";
    public static String PLAYER_UUID_DEFAULT = "0";
    public static String TABLERO_KEY = "tablero";
    public static String TABLERO_DEFAULT = "";
    public static String LOCAL_KEY = "conexion";
    public static boolean LOCAL_DEFAULT = true;
    public static boolean continuar;
    public static final String GAME_ID = "6";

    /**
     * Metodo que crea la actividad
     * @param savedInstanceState La instancia salvada
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_control_tamanio);
        fmanager = getFragmentManager();

        continuar = false;
        /* Iniciamos la reproduccion de la musica solo si en las preferencias esta activada */
        musicaFondo = MediaPlayer.create(getApplicationContext(), R.raw.mystifying_melodies);
        musicaFondo.setLooping(true);
        musicaFondo.seekTo(0);
        musicaFondo.start();

        if (!getValorMusica(getApplicationContext())) {
            musicaFondo.pause();
        }

        /* Recogemos  las metricas del dispositivo */
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics();
        display.getMetrics(outMetrics);

        /* Calculamos el ancho en dps del dispositivo */
        float density = getResources().getDisplayMetrics().density;
        float dpWidth = outMetrics.widthPixels / density;

        if (fmanager.findFragmentById(R.id.anchor_fragment1) == null) {

            MenuPrincipalFragment menuFragment = new MenuPrincipalFragment();
            transaccion = fmanager.beginTransaction();
            transaccion.add(R.id.anchor_fragment1, menuFragment);
            transaccion.commit();
        }

        if (dpWidth >= MAXDIMEN) {

            tableta = true;

            transaccion = fmanager.beginTransaction();

            if (fmanager.findFragmentById(R.id.anchor_fragment2) == null) {

                PartidaFragment partidaFragment = new PartidaFragment();
                transaccion.add(R.id.anchor_fragment2, partidaFragment).commit();
            }
        }
    }
    /**
     * Oncreate para el menu
     *
     * @param menu Menu a crear
     * @return Booleano indicando si se ha realizado con exito
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_juego, menu);

        if(tableta){
            MenuItem myItem = this.menu.findItem(R.id.action_cambiofichas);
            myItem.setVisible(true);
        }else {
            MenuItem myItem = this.menu.findItem(R.id.action_cambiofichas);
            myItem.setVisible(false);
        }

        return true;
    }

    /**
     * Listener para las opciones del menu
     *
     * @param item El menu a recibir
     * @return Un booleano que indica si se ha podido crear o no
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.logoutmenu:
                compruebaGuardarTablero(getApplicationContext());
                setNombreJugador(getApplicationContext(), NOMBRE_JUGADOR_DEFAULT);
                setPasswordJugador(getApplicationContext(), PASS_JUGADOR_DEFAULT);
                Intent intento = new Intent(PrincipalActivity.this, LoginActivity.class);
                startActivity(intento);
                PrincipalActivity.this.finish();
                return true;
            case R.id.action_cambiofichas:

                Fragment fragmentoPartida = getFragmentActual();
                ((PartidaFragment) fragmentoPartida).cambioFichas();
                if(((PartidaFragment) fragmentoPartida).getEstadoView() == TipoJuego.SIMPLE)
                    Toast.makeText(getApplicationContext(), R.string.cambiada_simple, Toast.LENGTH_SHORT).show();
                else
                    Toast.makeText(getApplicationContext(), R.string.cambiada_starwars, Toast.LENGTH_SHORT).show();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Sobreescritura de onPause para poder pausar la musica
     */
    @Override
    protected void onPause() {
        musicaFondo.pause();
        super.onPause();
    }

    /**
     * Sobreescritura de onResume para continuar con la reproduccion de la musica
     */
    @Override
    protected void onResume() {
        musicaFondo.start();
        super.onResume();
    }

    /**
     * Metodo que coloca un fragmento en la posicion correspondiente
     * @param fragmento El fragmento a colocar
     */
    private void colocaFragment(Fragment fragmento){

        if(fragmento instanceof PartidaFragment) {
            MenuItem myItem = this.menu.findItem(R.id.action_cambiofichas);
            myItem.setVisible(true);
        }else{

            MenuItem myItem = this.menu.findItem(R.id.action_cambiofichas);
            myItem.setVisible(false);

        }

        transaccion = fmanager.beginTransaction();

        if (tableta)
            transaccion.replace(R.id.anchor_fragment2, fragmento);
        else
            transaccion.replace(R.id.anchor_fragment1, fragmento);

        transaccion.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaccion.addToBackStack(null).commit();
    }
    /**
     * Metodo listener que controla la pulsacion de los botones del fragmento
     * @param seleccion El numero del boton seleccionado
     */
    @Override
    public void onBotonSeleccionado(TipoBoton seleccion) {

        switch (seleccion) {

            case NUEVA_PARTIDA:

                if(getLocalState(this)){
                    setTableroGuardado(getApplicationContext(), TABLERO_DEFAULT);
                    continuar = false;
                    Fragment partidaActual = getFragmentActual();

                    /** Si el fragmento actual es de tipo Partida, reiniciamos la partida */
                    if(partidaActual instanceof PartidaFragment){
                        ((PartidaFragment)partidaActual).reiniciar();
                    }
                    else{
                        continuar=false;
                        PartidaFragment partidaFragment = new PartidaFragment();
                        colocaFragment(partidaFragment);
                    }
                }else{

                    db = RepositoryFactory.createRepository(this);

                    Repository.StringResponseCallback callback = new Repository.StringResponseCallback() {
                        @Override
                        public void onResponse(String response) {

                            Toast.makeText(PrincipalActivity.this, "Partida creada con exito con ID: " + response, Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onError(String error) {

                            Toast.makeText(PrincipalActivity.this, "No se ha podido crear la partida: " + error, Toast.LENGTH_LONG).show();

                        }
                    };
                    db.newRound(getPlayerUUID(this), callback);

                }

                break;

            case CARGAR_PARTIDA:
                if(getTableroGuardado(getApplicationContext()).equals(TABLERO_DEFAULT))
                    Toast.makeText(getApplicationContext(),R.string.errorguardada,
                            Toast.LENGTH_SHORT).show();
                else {
                    
                    continuar = true;
                    /** Obtenemos el fragmento actual */
                    PartidaFragment fragmentPartida = new PartidaFragment();
                    colocaFragment(fragmentPartida);
                }
                break;

            case PREFERENCIAS:
                /** Guardamos el tablero si es necesario */
                compruebaGuardarTablero(getApplicationContext());
                PreferenciasFragment prefFragment = new PreferenciasFragment();
                colocaFragment(prefFragment);
                break;

            case ESTADISTICAS:
                compruebaGuardarTablero(getApplicationContext());
                EstadisticasFragment statsFragment = new EstadisticasFragment();
                colocaFragment(statsFragment);
                break;

            case INSTRUCCIONES:
                compruebaGuardarTablero(getApplicationContext());
                InstruccionesFragment instruccionesFragment = new InstruccionesFragment();
                colocaFragment(instruccionesFragment);
                break;

            case PARTIDAS_ONLINE:
                compruebaGuardarTablero(getApplicationContext());
                ListaPartidasOnlineFragment listaOnlineFragment = new ListaPartidasOnlineFragment();
                colocaFragment(listaOnlineFragment);
                break;


        }
    }

    /**
     * Sobreescritura del metodo onBackPressed para controlar las llamadas a los fragmentos
     */
    @Override
    public void onBackPressed() {
        int count = getFragmentManager().getBackStackEntryCount();

        if (count == 0)
            super.onBackPressed();

        else {

            /** Recuperamos el fragmento que hay cargado, ya sea tablet o movil */
            Fragment fragmentoActual = getFragmentActual();

            /** Si es de tipo tablero y esta en curso, lo almacenamos en preferencias */
            if ((fragmentoActual instanceof PartidaFragment)) {
                if (((PartidaFragment) fragmentoActual).geEstadoTablero() == Tablero.EN_CURSO) {
                    setTableroGuardado(getApplicationContext(), ((PartidaFragment) fragmentoActual).tableroToString());
                }
                ((PartidaFragment) fragmentoActual).startCrhonometer();
            }
            getFragmentManager().popBackStack();
        }
    }

    /**
     * Metodo listener de Preference que se activa cuando sucede un cambio en alguna de las
     * preferencias. Implementa funcionalidad para controlar la musica y el tipo de juego
     *
     * @param preference Preferencia que ha sido cambiada
     * @param newValue   El nuevo valor de la preferencia
     * @return true si la ejecucion ha salido correctamente, false en caso de error
     */

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {

        String key = preference.getKey();
        switch (key) {
            case KEY_MUSICA:

                boolean opcion = ((boolean) newValue);
                if (!opcion && musicaFondo.isPlaying())
                    musicaFondo.pause();
                else if (opcion && !musicaFondo.isPlaying()) {
                        musicaFondo.seekTo(0);
                        musicaFondo.start();
                }
                break;
            case KEY_EFECTOS:
                break;

            case KEY_ELECCION_FICHA:
                break;
            default:
        }
        return true;
    }

    /**
     * Metodo que devuelve el fragmento principal actual
     * @return El fragmento principal
     */
    private Fragment getFragmentActual(){
        return fmanager.findFragmentById(R.id.anchor_fragment2) == null ?
                fmanager.findFragmentById(R.id.anchor_fragment1) : fmanager.findFragmentById(R.id.anchor_fragment2);


    }

    /**
     * Comprueba el fragmento que esta puesto y si es el de una partida lo guarda en preferencias
     * en el caso de que la partida este en curso y no este vacia
     * @param context El contexto de la llamada
     * @return True si se ha podido guardar la partida, false en caso contrario
     */
    private boolean compruebaGuardarTablero(Context context) {
        Fragment fragmento = getFragmentActual();
        if (fragmento instanceof PartidaFragment) {
            PartidaFragment partida = (PartidaFragment)fragmento;
            if (partida.geEstadoTablero() == Tablero.EN_CURSO && !partida.isTableroVacio()){
                setTableroGuardado(context, ((PartidaFragment) fragmento).tableroToString());
                return true;
            }
        }
        return false;
    }
    /**
     * Getter de la preferencia de efectos de sonido
     *
     * @param context Contexto de la aplicacion
     * @return true en caso de estar activados, false en el contrario
     */

    public static boolean getValorEfectos(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_EFECTOS, EFECTOS_DEFAULT);
    }

    /**
     * Getter de la preferencia de la musica
     *
     * @param context Contexto de la aplicacion
     * @return true en caso de estar activados, false en el contrario
     */

    public static boolean getValorMusica(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(KEY_MUSICA, MUSICA_DEFAULT);
    }

    /**
     * Getter de la preferencia de eleccion de ficha
     *
     * @param context Contexto de la aplicacion
     * @return Entero que indica que ficha se ha elegido
     */

    public static String getEleccionFicha(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_ELECCION_FICHA, ELECCION_FICHA_DEFAULT);
    }


    /**
     * Setter del la eleccion de la ficha
     *
     * @param context Contexto de la aplicacion
     * @param ficha La ficha a guardar
     */
    public static void setEleccionFicha(Context context, TipoJuego ficha){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(TABLERO_KEY, ficha.toString());
        editor.commit();
    }
    /**
     * Getter del nombre del jugadore
     *
     * @param context Contexto de la aplicacion
     * @return String con el nombre del jugador
     */
    public static String getNombreJugador(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_NOMBRE_JUGADOR, NOMBRE_JUGADOR_DEFAULT);
    }

    /**
     * GSetter del nombre de jugador
     *
     * @param context Contexto de la aplicacion
     * @param nombrejugador El nombre del jugador a settear
     */
    public static void setNombreJugador(Context context, String nombrejugador){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_NOMBRE_JUGADOR, nombrejugador);
        editor.commit();
    }

    /**
     * Getter del nombre del jugadore
     *
     * @param context Contexto de la aplicacion
     * @return String con el nombre del jugador
     */
    public static String getPasswordJugador(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_PASS_JUGADOR, PASS_JUGADOR_DEFAULT);
    }

    /**
     * Setter del password del jugador
     *
     * @param context Contexto de la aplicacion
     * @param password La contraseña a poner
     */
    public static void setPasswordJugador(Context context, String password){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PASS_JUGADOR, password);
        editor.commit();

    }

    /**
     * Getter del UUID
     *
     * @param context Contexto de la aplicacion
     * @return El player UUID
     */
    public static String getPlayerUUID(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_PLAYER_UUID, PLAYER_UUID_DEFAULT);
    }

    /**
     * Setter del UUID
     *
     * @param context Contexto de la aplicacion
     * @param uuid El id del jugador
     */
    public static void setPlayerUUID(Context context, String uuid){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(KEY_PLAYER_UUID, uuid);
        editor.commit();
    }
    /**
     * Comprueba si el usuario tiene su login en preferencias
     *
     * @param context Contexto de la aplicacion
     * @return TRUE si esta, FALSE si no
     */
    public static boolean compruebaLogin(Context context){

        return !getNombreJugador(context).equals(PrincipalActivity.NOMBRE_JUGADOR_DEFAULT)
                && !getPasswordJugador(context).equals(PrincipalActivity.PASS_JUGADOR_DEFAULT);
    }

    /**
     * Setter del tablero anterior guardado
     *
     * @param context Contexto de la aplicacion
     * @param tablero El tablero a guardar
     */
    public static void setTableroGuardado(Context context, String tablero){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(TABLERO_KEY, tablero);
        editor.commit();
    }

    /**
     * Setter del tablero anterior guardado
     *
     * @param context Contexto de la aplicacion
     * @return El tablero guardado
     */
    public static String getTableroGuardado(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(TABLERO_KEY, TABLERO_DEFAULT);

    }

    /**
     * Setter de la preferencia de modo de juego
     *
     * @param context Contexto de la aplicacion
     * @param local El modo a settear
     */
    public static void setLocalState(Context context, boolean local){

        SharedPreferences sp= PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sp.edit();
        editor.putBoolean(LOCAL_KEY, local);
        editor.commit();
    }

    /**
     * Getter de la preferencia de modo de juego
     *
     * @param context Contexto de la aplicacion
     * @return El tablero guardado
     */
    public static boolean getLocalState(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(LOCAL_KEY, LOCAL_DEFAULT);

    }

}