package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;

public class Dimension {
	private Integer x;
	private Integer y;

	public Dimension(Integer x, Integer y) {
		this.x = x;
		this.y = y;
	}

	public Integer getX() {
		return x;
	}

	public Integer getY() {
		return y;
	}

	/** Sobreescritura del metodo equals */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Dimension))
			return false;

		Dimension other = (Dimension) obj;
		if (x == other.getX() && y == other.getY())
			return true;

		return false;
	}

	/** toString de la Dimension */
	@Override
	public String toString() {
		return "(" + this.getX() + "," + this.getY() + ")";

	}
}
