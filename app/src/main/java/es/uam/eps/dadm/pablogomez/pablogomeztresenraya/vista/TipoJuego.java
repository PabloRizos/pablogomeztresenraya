/**
 * Enumeracion que identifica el tipo de juego que se esta jugando
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista;


public enum TipoJuego {
    SIMPLE("0"), STARWARS("1");

    private final String txt;

    TipoJuego(String txt) {
        this.txt = txt;
    }

    @Override
    public String toString() {
        return txt;
    }
}
