package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.ListaPartidasOnlineFragment;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador.PrincipalActivity;
import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.db.Repository;

public class MiArrayAdapterOnline extends ArrayAdapter<Repository.PartidaJSON>{

    private class Holder {
        TextView tvidRonda;
        Button botonLista;
    }

    public MiArrayAdapterOnline(Context context) {
        super(context, R.layout.mi_list_item_online);
    }

    /**
     * Versión más eficiente. Evita también el uso de findViewById guardando una referencia directa
     * a los elementos del layout fila que necesitamos modificar. Este método puede ser entorno a un
     * 15% más rápido que el anterior.
     *
     */
    public View getView(int position, View convertView, ViewGroup parent) {

        View vistaLinea;
        Holder holder;

        vistaLinea = convertView;
        if ( vistaLinea == null ) {
            LayoutInflater inflater = (LayoutInflater) getContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            vistaLinea = inflater.inflate(R.layout.mi_list_item_online, parent, false);
            // Guardamos en esta clase la referencia directa al TextView e ImageView
            holder = new Holder();
            holder.tvidRonda = (TextView) vistaLinea.findViewById(R.id.idRonda);
            holder.botonLista = (Button) vistaLinea.findViewById(R.id.boton_listaonline);
            // El holder se guarda en el Tag de la Vista, que sirve para almacenar un Object
            vistaLinea.setTag(holder);
        }

        // Recuperamos el holder de las filas ya creadas
        holder = (Holder)vistaLinea.getTag();
        Log.i("DEBUG :::", "Round id: " + getItem(position).roundid);
        String textoTv = vistaLinea.getResources().getString(R.string.partidaDe) + " " + getItem(position).playernames;
        holder.tvidRonda.setText(textoTv);

        if(getItem(position).playernames.equals(PrincipalActivity.getNombreJugador(getContext())))
            holder.botonLista.setBackgroundResource(R.drawable.ic_botonpapelera);
        else
            holder.botonLista.setBackgroundResource(R.drawable.ic_play_button_blue);


        holder.botonLista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int posicionClick = ListaPartidasOnlineFragment.
                                    getListViewAbiertas().getPositionForView(v);
                getItem(posicionClick);
            }
        });
        return vistaLinea;
    }
}
