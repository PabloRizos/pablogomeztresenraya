/**
 * Actividad que mostrará la pantalla de instrucciones
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   31-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class InstruccionesFragment extends Fragment {


    public InstruccionesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_instrucciones, container, false);
    }

}
