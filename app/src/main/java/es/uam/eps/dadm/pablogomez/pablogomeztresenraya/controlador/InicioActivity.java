/**
 * Actividad inicial de la app. Tan solo consta de un texto y un boton para avanzar a la siguiente
 * actividad: PrincipalActivity
 *
 * @author  Pablo Gomez Delgado
 * @version 1.1
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.controlador;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;
import android.widget.TextView;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;

public class InicioActivity extends AppCompatActivity {

    private ProgressBar mProgress;
    private boolean stop=false;
    private int mProgressStatus = 0;
    private Thread hilo;

    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null)
            savedInstanceState.getInt("PROGRESS", mProgressStatus);
        setContentView(R.layout.activity_inicio);

        Typeface myTypeface = Typeface.createFromAsset(getAssets(), "fonts/robotfont.ttf"); /** Fuente propia para el inicio */
        TextView textView = (TextView) findViewById(R.id.textoinicio);
        assert textView != null;
        textView.setTypeface(myTypeface);

        mProgress = (ProgressBar) findViewById(R.id.progressBar);

        /* Start lengthy operation in a background thread */
        hilo = new Thread(new Runnable() {
            public void run() {
                while (mProgressStatus < 100) {
                    mProgressStatus += 5;
                    if(mProgressStatus>=100){

                        Intent intento;
                        /** Si ya existe un usuario loggeado */
                        if(PrincipalActivity.compruebaLogin(getApplicationContext()))
                            /** Iniciamos la actividad principal */
                            intento = new Intent(InicioActivity.this, PrincipalActivity.class);
                        else
                            /** Iniciamos el login */
                            intento = new Intent(InicioActivity.this, LoginActivity.class);
                        startActivity(intento);
                        InicioActivity.this.finish();
                    }
                    SystemClock.sleep(200);

                    // Update the progress bar
                    mHandler.post(new Runnable() {
                        public void run() {
                            mProgress.setProgress(mProgressStatus);
                        }
                    });
                }
            }
        });
        hilo.start();
    }

    /**
     * Sobreescritura del metodo onSaveInstanceState
     * @param outState El bundle a guardar
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("PROGRESS", mProgressStatus);
        super.onSaveInstanceState(outState);
    }

    /**
     * Sobreescritura del metodo onPause
     */
    @Override
    protected void onPause() {
        hilo.interrupt();
        super.onPause();
    }
}

