/**
 * Vista de una ficha, ya sea pintada o de tipo Bitmap. Se utiliza para mostrar las miniaturas
 * de las mismas
 *
 * @author  Pablo Gomez Delgado
 * @version 1.0
 * @since   25-03-2016
 */
package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.vista;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import es.uam.eps.dadm.pablogomez.pablogomeztresenraya.R;

/**
 * Clase que representa una sola ficha para su muestra en la pantalla principal
 * Created by pablo on 8/3/16.
 */
public class FichaView extends View {

    private TipoJuego tipo;
    private int ficha;
    private int alto;
    private int ancho;
    private float[] posiciones;
    private Rect posicion;
    private Paint ficha1;
    private Paint ficha2;
    private Bitmap ficha1_sw;
    private Bitmap ficha2_sw;



    /* Constructores de la clase View */
    public FichaView(Context context) {
        this(context, null, 0);
    }

    public FichaView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FichaView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * Metodo que inicializa todos los atributos necesarios para pintar la vista con OnDraw
     */
    private void init(){

        /* Obtenemos las dimensiones del control */
        alto = getMeasuredHeight();
        ancho = getMeasuredWidth();

        posiciones = new float[4];
        ficha1 = new Paint();
        ficha2 = new Paint();
        ficha1_sw = BitmapFactory.decodeResource(getResources(), R.drawable.luke);
        ficha2_sw = BitmapFactory.decodeResource(getResources(), R.drawable.vader);

        ficha2.setStyle(Paint.Style.STROKE);
        ficha2.setStrokeWidth(8);
        ficha2.setColor(ContextCompat.getColor(getContext(), R.color.granate));

        posiciones[0] = ancho * 0.25f;
        posiciones[1] = alto * 0.25f;
        posiciones[2] = ancho * 0.75f;
        posiciones[3] = alto * 0.75f;

        posicion = new Rect(
                (int) (posiciones[0] * 1.35f),
                (int) (posiciones[1] * 1.35f),
                (int) (posiciones[2] * 1.35f),
                (int) (posiciones[3] * 1.35f));
    }
    /**
     * Metodo que pinta en la pantalla el tablero con sus fichas
     * Parte de Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
     * @param canvas El lienzo sobre el que pintar
     */
    @Override
    protected void onDraw(Canvas canvas) {

        if(tipo == TipoJuego.SIMPLE && ficha==0){


            ficha1.setStyle(Paint.Style.STROKE);
            ficha1.setStrokeWidth(8);
            ficha1.setColor(ContextCompat.getColor(getContext(), R.color.azulomedio));

            canvas.drawLine(
                    posiciones[0],
                    posiciones[1],
                    posiciones[2],
                    posiciones[3],
                    ficha1);

            canvas.drawLine(
                    posiciones[2],
                    posiciones[1],
                    posiciones[0],
                    posiciones[3],
                    ficha1);

        }else if(tipo == TipoJuego.SIMPLE && ficha==1){

            canvas.drawCircle(
                    ancho * 0.5f,
                    alto * 0.5f,
                    ancho * 0.3f, ficha2);

        }
        else if(tipo == TipoJuego.STARWARS && ficha == 0){

            canvas.drawBitmap(ficha1_sw, null, posicion, null);

        }
        else if(tipo == TipoJuego.STARWARS && ficha == 1){

            canvas.drawBitmap(ficha2_sw, null, posicion, null);

        }
    }
    /**
     * Reescribimos onLayout debido a que se llama despues de onMeasure, con lo que nos aseguramos
     * de tener ya las dimensiones de la pantalla (ancho y alto) bien recogidas
     */
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        init();
    }

    /**
   * Metodo para mantener la proporcion de la vista
   * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
   */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int ancho = calcularAncho(widthMeasureSpec);
        int alto = calcularAlto(heightMeasureSpec);

        if(ancho < alto)
            alto = ancho;
        else
            ancho = alto;

        setMeasuredDimension(ancho, alto);
    }
    /**
    * Metodo para calcular el alto de la vista
    * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
    */
    private int calcularAlto(int limitesSpec)
    {
        int res = 100; //Alto por defecto

        int modo = MeasureSpec.getMode(limitesSpec);
        int limite = MeasureSpec.getSize(limitesSpec);

        if (modo == MeasureSpec.AT_MOST) {
            res = limite;
        }
        else if (modo == MeasureSpec.EXACTLY) {
            res = limite;
        }

        return res;
    }
    /*
    * Metodo para calcular el ancho de la vista
    * Codigo de SGoLiver: http://www.sgoliver.net/blog/interfaz-de-usuario-en-android-controles-personalizados-iii/
    */
    private int calcularAncho(int limitesSpec)
    {
        int res = 100; //Ancho por defecto

        int modo = MeasureSpec.getMode(limitesSpec);
        int limite = MeasureSpec.getSize(limitesSpec);

        if (modo == MeasureSpec.AT_MOST) {
            res = limite;
        }
        else if (modo == MeasureSpec.EXACTLY) {
            res = limite;
        }

        return res;
    }

    /** Setters de la clase */
    public void setTipo(TipoJuego tipo) {
        this.tipo = tipo;
    }

    public void setFicha(int ficha) {
        this.ficha = ficha;
    }
}
