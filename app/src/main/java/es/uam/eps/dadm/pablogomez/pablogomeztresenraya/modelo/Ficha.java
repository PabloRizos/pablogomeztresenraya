package es.uam.eps.dadm.pablogomez.pablogomeztresenraya.modelo;

public enum Ficha {
	VACIO(" "), TIPO1("X"), TIPO2("O");

	private final String txt;

	private Ficha(String txt) {
		this.txt = txt;
	}

	@Override
	public String toString() {
		return txt;
	}
}
